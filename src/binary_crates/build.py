#!/usr/bin/env python3

# Copyright (C) 2023 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import os
from pathlib import Path
import sys


sys.path.append(str(Path(__file__).parent.parent.resolve()))
from android_rust.cargo import *


def parse_args(argv: list[str] | None) -> argparse.Namespace:
    """Parses arguments and returns the parsed structure."""
    parser = argparse.ArgumentParser("Import binary crate")
    parser.add_argument("--crate-path", help="Path to the imported crate")
    parser.add_argument("--upstream-test", action="store_true", help="Run 'cargo test'")
    parser.add_argument(
        "--audit",
        action="store_true",
        help=(
            "Check the structure of the crate directory, and run 'cargo deny' and"
            " 'cargo vet'"),
    )
    # This script relies on having "cargo deny" and "cargo vet" available, but they are
    # themselves binary crates that we need to import. So, for bootstrapping, permit
    # the use of versions installed for the system default Rust toolchain.
    # TODO(jamesfarrell): Remove once cargo-vet and cargo-deny are part of the prebuilt toolchain.
    parser.add_argument(
        "--use-system-cargo-plugins",
        action="store_true",
        help=(
            "Use cargo plugins installed by the system default cargo, usually in"
            " $HOME/.cargo/bin"),
    )
    args = parser.parse_args(argv)
    return args


def raise_unless_dir(path: Path) -> None:
    if not path.is_dir():
        raise RuntimeError(f"{path} does not exist, or is not a directory.")


def run_audits(crate: Crate) -> None:
    raise_unless_dir(crate.crate_path / "android")
    raise_unless_dir(crate.crate_path / "android" / "vendor")
    raise_unless_dir(crate.crate_path / "android" / "supply-chain")

    crate.cargo_deny()
    crate.cargo_vet()


def main(argv: list[str] | None = None) -> int:
    args = parse_args(argv)
    env = dict(os.environ)

    # rustup adds ~/.cargo/bin to your path, which means that cargo plugins will work with the prebuilt cargo
    # even if they are installed in ~/.cargo/bin. So we remove cargo's path, unless instructed otherwise.
    if not args.use_system_cargo_plugins:
        remove_cargo_from_path(env)

    crate_path: Path = Path(args.crate_path).resolve()
    raise_unless_dir(crate_path)
    crate = Crate(crate_path, env)

    if args.audit:
        run_audits(crate)

    if args.upstream_test:
        crate.cargo_test()

    crate.cargo_build()

    return 0


if __name__ == "__main__":
    sys.exit(main())
