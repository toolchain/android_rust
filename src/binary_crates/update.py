#!/usr/bin/env python3

# Copyright (C) 2023 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Instructions:
#
# 1. repo start update-cargo-deny toolchain/cargo-deny
#
# 2. prebuilts/python/linux-x86/bin/python3 toolchain/android_rust/binary_crates/update.py --crate-path=toolchain/cargo-deny --branch=0.13.9 --upstream=https://github.com/EmbarkStudios/cargo-deny.git
#
# 3. repo upload toolchain/cargo-deny -o nokeycheck -o banned-words~skip --cbr
#
# Other things you can do:
#
# prebuilts/python/linux-x86/bin/python3 toolchain/android_rust/binary_crates/import.py --crate-path=toolchain/cargo-vet
# prebuilts/python/linux-x86/bin/python3 toolchain/android_rust/binary_crates/import.py --crate-path=toolchain/sccache --branch=v0.5.1 --upstream=https://github.com/mozilla/sccache.git

import argparse
import os
from pathlib import Path
import re
import shutil
import sys


sys.path.append(str(Path(__file__).parent.parent.resolve()))
from android_rust.cargo import *
from android_rust.utils import GitRepo


def parse_args(argv: list[str] | None) -> argparse.Namespace:
    """Parses arguments and returns the parsed structure."""
    parser = argparse.ArgumentParser("Import or update a binary crate")
    parser.add_argument("--crate-path", help="Path to the imported crate")
    parser.add_argument(
        "--branch",
        default="aosp/upstream-main",
        help="Branch or tag to merge from",
    )
    parser.add_argument("--upstream", help="Upstream repo to fetch tags from")
    parser.add_argument("--upstream-test", action="store_true", help="Run 'cargo test'")
    parser.add_argument(
        "--num-vendor-commits",
        default=3,
        type=int,
        help="Use this many separate commits for vendored dependencies",
    )
    parser.add_argument("--bug", help="Bug ID to include in commit messages")
    parser.add_argument(
        "--skip-merge",
        action="store_true",
        help="Don't merge from upstream, just update Android customizations.",
    )
    # This script relies on having "cargo deny" and "cargo vet" available, but they are
    # themselves binary crates that we need to import. So, for bootstrapping, permit
    # the use of versions installed for the system default Rust toolchain.
    # TODO(jamesfarrell): Remove once cargo-vet and cargo-deny are part of the prebuilt toolchain.
    parser.add_argument(
        "--use-system-cargo-plugins",
        action="store_true",
        help=(
            "Use cargo plugins installed by the system default cargo, usually in"
            " $HOME/.cargo/bin"),
    )
    args = parser.parse_args(argv)
    return args


def update(argv: list[str] | None = None, initial_import: bool = False) -> int:
    args = parse_args(argv)
    env = dict(os.environ)

    # rustup adds ~/.cargo/bin to your path, which means that cargo plugins will work with the prebuilt cargo
    # even if they are installed in ~/.cargo/bin. So we remove cargo's path, unless instructed otherwise.
    if not args.use_system_cargo_plugins:
        remove_cargo_from_path(env)

    crate_path: Path = Path(args.crate_path).resolve()
    if not crate_path.is_dir():
        print(f"{args.crate_path} does not exist, or is not a directory.", file=sys.stderr)
        return 1
    git = GitRepo(crate_path, env=env)

    commit_message_extra = "\n\nTest: treehugger"
    if args.bug:
        commit_message_extra = f"\nBug: {args.bug}"

    git.fetch("aosp")
    if args.upstream:
        git.remote_add("upstream", args.upstream)

    if not args.skip_merge:
        print(f"Merging {args.branch} into {args.crate_path}")
        git.merge(args.branch, f"Merge '{args.branch}'{commit_message_extra}")

    android_dir = crate_path / "android"
    if initial_import:
        # Create an android directory to store vendored deps, audits, and other tweaks.
        android_dir.mkdir(exist_ok=False)
    if not android_dir.is_dir():
        raise RuntimeError(f"{android_dir} does not exist, or is not a directory.")

    crate = Crate(crate_path, env)

    crate.cargo_vendor()

    crate.cargo_deny()

    if not initial_import:
        # At the moment, we are not doing any audits. So we just blow it away and re-initialize
        # with all dependencies exempted.
        shutil.rmtree(android_dir / "supply-chain")
    crate.cargo_vet_init()
    crate.cargo_vet_prune()

    if args.upstream_test:
        crate.cargo_test()

    if initial_import:
        # Disable EOL conversions in vendored code.
        with open(android_dir / ".gitattributes", "w") as gitattributes:
            gitattributes.write("vendor/** -text diff")

        # Some vendored dependencies may contain false positives for `git secrets`.
        if (crate_path / ".gitallowed").exists():
            raise RuntimeError(".gitallowed already exists")
        with open(crate_path / ".gitallowed", "w") as gitallowed:
            gitallowed.write("android/vendor/")

    print("Committing changes")
    commit_prefix = (
        f"Initial import of {crate_path.name}"
        if initial_import else f"Android updates for {crate_path.name}")
    if args.num_vendor_commits == 0:
        git.add(".")
        git.commit(f"{commit_prefix}{commit_message_extra}")
    else:
        # Add and commit everything except the vendored deps in android/vendor.
        if len(git.status(".", ":!^android/vendor")) > 0:
            git.add(".", ":!^android/vendor")
            git.commit(f"{commit_prefix}{commit_message_extra}")

        if initial_import:
            all_crates = [f for f in os.listdir(crate_path / "android" / "vendor")]
        else:
            all_crates = [
                c for c in re.findall(
                    r"^.. android/vendor/([^/]*)",
                    git.status("android/vendor"),
                    flags=re.MULTILINE,
                )
            ]
        changed_crates: dict[str, set[str]] = {}
        for crate_and_version in all_crates:
            match = re.match(r"([^/]*)-([^-/]*)", crate_and_version)
            if not match:
                raise RuntimeError(
                    f"RE match for pattern and version failed for {crate_and_version}")
            (crate_name, version) = match.group(1, 2)
            if crate_name not in changed_crates:
                changed_crates[crate_name] = set()
            changed_crates[crate_name].add(version)
        changed_crate_names = list(changed_crates.keys())
        num_crates = len(changed_crate_names)
        if num_crates > 0:
            chunks = [
                changed_crate_names[i * num_crates // args.num_vendor_commits:(i + 1) *
                                    num_crates // args.num_vendor_commits]
                for i in range(args.num_vendor_commits)
            ]
            for i, chunk in enumerate(chunks):
                for crate_name in chunk:
                    git.add(
                        *[
                            Path("android/vendor/") / f"{crate_name}-{v}" for v in changed_crates[crate_name]
                        ])
                git.commit(
                    f"Vendored dependencies for {crate_path.name}, part"
                    f" {i+1}/{args.num_vendor_commits}{commit_message_extra}")

    # Detect ignored files
    if git.has_ignored_files("android"):
        print(
            f"{args.crate_path} has .gitignored files in the android/ directory.",
            file=sys.stderr,
        )
        return 1

    return 0


if __name__ == "__main__":
    sys.exit(update())
