# Copyright (C) 2024 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This is intended to re-export the `llvm_android` build scripts as a library,
which allows us to re-use things like their zstd build support.
"""

from pathlib import Path

__path__ = [str((Path(__file__).parent.parent.parent.parent / "llvm_android" / "src" / "llvm_android").resolve())]
