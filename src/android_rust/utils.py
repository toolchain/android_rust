# Copyright (C) 2021 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Code common to different scripts in the Rust toolchain"""

import argparse
import os
from pathlib import Path
import re
import shlex
import shutil
import stat
from string import Template
import sys
import subprocess
import tempfile
from typing import Any, Optional, TextIO, TypedDict, Tuple, Union, cast

import android_rust.build_platform as build_platform
from android_rust.paths import (
    BIN_PATH_FILE,
    BIN_PATH_TAR,
    BIN_PATH_XZ,
    OUT_PATH,
    RUST_PREBUILT_PATH,
    SOONG_UI_PATH,
    LINK_PATH_UNZIP,
    WORKSPACE_PATH)


class RunQuietArgDct(TypedDict):
    stdout: int
    stderr: int


class ScriptException(Exception):

    """
    Indicates that the arguments given either don't work or don't make sense
    in the context of the system state
    """

    def __init__(self, message: str):
        self.message = message
        super().__init__(message)


GIT_REFERENCE_BRANCH = "aosp/main"

SUBPROCESS_RUN_QUIET_DEFAULTS: RunQuietArgDct = {
    "stdout": subprocess.DEVNULL,
    "stderr": subprocess.DEVNULL,
}

TERM_END = "\033[0m"
TERM_YELLOW = "\033[93m"
TERM_GREEN = "\033[92m"
TERM_RED = "\033[91m"

TEST_VERSION_NUMBER: str = "9.99.9"

VERSION_PATTERN = re.compile("\d+\.\d+\.\d+(\.p\d+)?")

#
# Type Functions
#


def version_string_type(arg_string: str) -> str:
    if VERSION_PATTERN.match(arg_string):
        return arg_string
    else:
        raise argparse.ArgumentTypeError("Version string is not properly formatted")


def ResolvedPath(arg: str) -> Path:
    return Path(arg).resolve()


def ExtantPath(arg: str) -> Path:
    if "*" in arg:
        search_root = Path("/") if arg.startswith("/") else Path.cwd()
        results = list(search_root.glob(arg))
        if len(results) == 0:
            raise argparse.ArgumentTypeError(f"No matching files for glob pattern {arg}")
        elif len(results) == 1:
            path = results[0].resolve()
        else:
            raise argparse.ArgumentTypeError(f"Multiple matching files for glob pattern {arg}")
    else:
        path = Path(arg).resolve()

    if path.exists():
        return path
    else:
        raise argparse.ArgumentTypeError(f"Path does not exist: {arg}")


#
# String Helpers
#


def print_colored(txt: str, color: str) -> None:
    print(f"{color}{txt}{TERM_END}")


def env_str_to_bool(txt: str | None) -> bool:
    return txt is not None and txt.lower() in ["true", "1"]


#
# Subprocess helpers
#


def prepare_command(command: Union[str, list[Any]]) -> list[str]:
    if isinstance(command, list):
        command_list: list[str] = [str(obj) for obj in command]
    else:
        command_list = shlex.split(command)

    if not Path(command_list[0]).exists():
        resolved_executable = shutil.which(command_list[0])
        if resolved_executable:
            command_list[0] = resolved_executable
        else:
            raise RuntimeError(f"Unable to find executable {command_list[0]}")

    return command_list


def run_and_exit_on_failure(
        command: Union[str, list[Any]], error_message: str, *args: Any,
        **kwargs: Any) -> subprocess.CompletedProcess[str]:
    """Runs a command where failure is a valid outcome"""
    command = prepare_command(command) if not kwargs.get("shell") else command
    result = subprocess.run(command, *args, **kwargs)
    if result.returncode != 0:
        sys.exit(error_message)

    return result


def run_quiet_and_exit_on_failure(
        command: Union[str, list[Any]], error_message: str, *args: Any, **kwargs: Any) -> int:
    """Runs a failable command with stdout and stderr directed to /dev/null"""
    return run_and_exit_on_failure(
        command, error_message, *args, **(kwargs | SUBPROCESS_RUN_QUIET_DEFAULTS)).returncode


def run_quiet(command: Union[str, list[Any]], *args: Any, **kwargs: Any) -> int:
    return subprocess.run(
        prepare_command(command), *args,
        **cast(Any, (kwargs | SUBPROCESS_RUN_QUIET_DEFAULTS))).returncode


#
# Android helpers
#

RUST_PREBUILT_NAME_PATTERN = re.compile("rust-(?!profraw)(\S*)\.tar\.xz")


def prepare_prebuilts(prebuilt_path: Path, reuse: bool) -> Path:
    # Prepare host/version path
    target_and_version_path: Path = RUST_PREBUILT_PATH / build_platform.prebuilt() / TEST_VERSION_NUMBER
    if target_and_version_path.exists():
        if reuse:
            return target_and_version_path
        else:
            print("Deleting contents of existing prebuilt directory.")
            shutil.rmtree(target_and_version_path, ignore_errors=True)

    prebuilt_path = resolve_prebuilt_path(prebuilt_path, RUST_PREBUILT_NAME_PATTERN)
    target_and_version_path.mkdir()

    # Unpack prebuilt
    print(f"Extracting archive {prebuilt_path}")
    archive_extract(prebuilt_path, target_and_version_path)
    # Update the mtime of the compiler to ensure Soong properly cleans out any
    # intermediate files compiled with a different compiler.
    (target_and_version_path / "bin" / "rustc").touch()

    return target_and_version_path


def resolve_prebuilt_path(arg_path: Path, name_pattern: re.Pattern[str]) -> Path:
    # Resolve prebuilt path
    resolved_path: Path = arg_path
    if resolved_path.exists():
        if resolved_path.is_dir():
            matches = [f for f in resolved_path.iterdir() if name_pattern.match(f.name)]
            if len(matches) > 1:
                sys.exit("Failed to resolve prebuilt path: multiple prebuilts present")

            resolved_path = matches.pop()

        return resolved_path.resolve()

    else:
        raise ScriptException("Failed to resolve prebuilt path: path doesn't exist")


def run_build_target(
    build_targets: Union[str, list[str]],
    device_target: str = "aosp_cf_x86_64_phone",
    release_name: str = "trunk_staging",
    device_target_variant: str = "userdebug",
    prebuilt_version: Optional[str] = None,
    env: dict[str, str] = os.environ.copy()) -> None:

    command_buffer = []

    if prebuilt_version:
        env["RUST_PREBUILTS_VERSION"] = prebuilt_version

    command_buffer = [str(SOONG_UI_PATH), "--make-mode"]

    if device_target:
        command_buffer.append(f"TARGET_PRODUCT={device_target}")
        command_buffer.append(f"TARGET_RELEASE={release_name}")
        command_buffer.append(f"TARGET_BUILD_VARIANT={device_target_variant}")

    match build_targets:
        case list(build_targets):
            for build_target in build_targets:
                command_buffer.append(build_target)
        case str(build_targets):
            command_buffer.append(build_targets)

    retcode = subprocess.run(command_buffer, stderr=subprocess.STDOUT, env=env).returncode
    if retcode != 0:
        raise ScriptException("Soong build invocation failed")


#
# Git
#


class GitRepo:
    run_kwargs: dict[Any, Any]

    def __init__(self, repo_path: Path, **kwargs: dict[Any, Any]) -> None:
        self.path = repo_path
        self.run_kwargs = {
            **kwargs, "cwd": self.path
        }

    def add(self, *patterns: Union[str, Path]) -> None:
        pretty_patterns = ", ".join([f"'{p}'" for p in patterns])
        run_quiet_and_exit_on_failure(
            ["git", "add", *patterns],
            f"Failed to add files matching {pretty_patterns} to Git repo {self.path}",
            **self.run_kwargs)

    def am(self, patch_list: list[Path], error_msg: str | None = None) -> None:
        message_buffer = f"Failed to apply mailbox patches as commits for Git repo {self.path}"
        if error_msg is not None:
            message_buffer += f"\n{error_msg}"

        run_and_exit_on_failure(
            ["git", "am", "-C1", "--ignore-space-change", "--ignore-whitespace", *patch_list],
            message_buffer,
            **self.run_kwargs)

    def amend(self) -> None:
        run_quiet_and_exit_on_failure(["git", "commit", "--amend", "--no-edit"],
                                      f"Failed to amend previous commit for Git repo {self.path}",
                                      **self.run_kwargs)

    def amend_or_commit(self, commit_message: str) -> None:
        if not self.diff():
            print("No files updated")
        elif (self.branch_target() != self.branch_target(GIT_REFERENCE_BRANCH)):

            print("Amending previous commit")
            self.amend()
        else:
            print("Committing new files")
            self.commit(commit_message)

    def branch_delete(self, branch_name: str) -> None:
        run_and_exit_on_failure(["git", "branch", "-D", branch_name],
                                f"Failed to delete branch {branch_name} for Git repo {self.path}",
                                **self.run_kwargs)

    def branch_exists(self, branch_name: str) -> bool:
        return run_quiet(self.branch_test_cmd(branch_name), **self.run_kwargs) == 0

    def branch_target(self, branch_name: str = "HEAD") -> str:
        return run_and_exit_on_failure(
            self.branch_test_cmd(branch_name),
            f"Failed to get target hash for branch '{branch_name}' of Git repo {self.path}",
            **{
                **self.run_kwargs, "stdout": subprocess.PIPE,
                "stderr": subprocess.DEVNULL
            },
        ).stdout.rstrip()

    def branch_test_cmd(self, branch_name: str) -> list[str]:
        return ["git", "rev-parse", "--verify", branch_name]

    def checkout(self, branch_name: str) -> None:
        run_quiet_and_exit_on_failure(
            ["git", "checkout", branch_name],
            f"Failed to checkout branch '{branch_name}' for Git repo {self.path}",
            **self.run_kwargs)

    def commit(self, message: str) -> None:
        run_quiet_and_exit_on_failure(["git", "commit", "-m", message],
                                      f"Failed to create commit for Git repo {self.path}",
                                      **self.run_kwargs)

    def create_or_checkout(self, branch_name: str, overwrite: bool) -> bool:
        """Create or checkout a branch, returning true if a new branch was created"""
        if self.branch_exists(branch_name):
            if overwrite:
                print(f"Checking out branch {branch_name}")
                self.checkout(branch_name)
                return False
            else:
                raise ScriptException(
                    f"Branch {branch_name} already exists and the 'overwrite' option was not set")
        else:
            print(f"Creating branch {branch_name}")
            if self.path.is_relative_to(WORKSPACE_PATH) and not self.path.is_relative_to(OUT_PATH):
                run_quiet_and_exit_on_failure(
                    ["repo", "start", branch_name],
                    f"Failed to 'repo start' branch '{branch_name}' for Git repo {self.path}",
                    cwd=self.path)
            else:
                run_quiet_and_exit_on_failure(
                    ["git", "checkout", "-b", branch_name, "aosp/main"],
                    f"Failed to create branch {branch_name} for Git repo {self.path}",
                    **self.run_kwargs)

            return True

    def diff(self) -> bool:
        retcode = run_quiet(["git", "diff", "--cached", "--quiet"], **self.run_kwargs)

        if retcode == 0:
            return False
        elif retcode == 1:
            return True
        else:
            raise RuntimeError("Failed to compute diff for Git repo {self.path}")

    def reset_hard(self, number: int) -> None:
        run_and_exit_on_failure(["git", "reset", "--hard", f"HEAD~{number}"],
                                f"Failed to run 'git reset --hard HEAD~{number}'",
                                **self.run_kwargs)

    def rm(self, *patterns: Union[str, Path], options: str = "frq") -> None:
        pretty_patterns = ", ".join([f"'{p}'" for p in patterns])
        run_quiet_and_exit_on_failure(
            ["git", "rm", f"-{options}", *patterns],
            f"Failed to remove files matching pattern(s) {pretty_patterns} from Git repo {self.path}",
            **self.run_kwargs)

    def fetch(self, branch: str) -> None:
        run_and_exit_on_failure(["git", "fetch", branch],
                                f"Failed to run 'git fetch {branch}'",
                                **self.run_kwargs)

    def format_patches(
            self,
            start: str = "aosp/main",
            outdir: Path | None = None,
            context_size: int = 10) -> None:
        command = [
            "git",
            "format-patch", # Extra context is needed for some of the patches to apply correctly
            f"-U{context_size}",
        ]

        if outdir:
            command.append("-o")
            command.append(str(outdir))

        command.append(start)

        run_and_exit_on_failure(
            command, f"Failed to generate patch files for Git repo {self.path}", **self.run_kwargs)

    def merge(self, branch: str, message: str) -> None:
        run_and_exit_on_failure(
            [
                "git",
                "merge",
                "--allow-unrelated-histories",
                "--no-edit",
                "-m",
                message,
                branch,
            ],
            f"Failed to run 'git merge {branch}'",
            **self.run_kwargs) # yapf: disable


    def has_ignored_files(self, path: Path | str) -> bool:
        ignored_files = subprocess.run(["git", "status", "--ignored", "--porcelain", str(path)],
                                       check=True,
                                       capture_output=True,
                                       **self.run_kwargs).stdout

        return len(ignored_files) > 0

    def remote_add(self, name: str, url: str) -> None:
        """Add an upstream branch, removing the existing one first if necessary"""
        run_quiet(["git", "remote", "remove", name], **self.run_kwargs)
        run_and_exit_on_failure(["git", "remote", "add", "-f", "--tags", name, url],
                                "Failed to run 'git remote add {name} {url}' to add upstream tags",
                                **self.run_kwargs)

    def status(self, *patterns: Union[str, Path]) -> str:
        return str(
            subprocess.run(["git", "status", "--porcelain", *[str(p) for p in patterns]],
                           check=True,
                           capture_output=True,
                           **self.run_kwargs).stdout.decode("utf-8"))


#
# File helpers
#


def is_elf_file(path: Path) -> bool:
    file_info = subprocess.run([BIN_PATH_FILE, path], stdout=subprocess.PIPE, text=True).stdout or ""
    return "ELF" in file_info


def replace_file_contents(f: TextIO, new_contents: str) -> None:
    f.seek(0)
    f.write(new_contents)
    f.truncate()
    f.flush()


def recursive_chown(path: Path, user: int, group: int) -> None:
    for root_name, _, file_names in os.walk(path):
        root_path = Path(root_name)
        os.chown(root_path, user, group, follow_symlinks=False)

        for name in file_names:
            os.chown(root_path / name, user, group, follow_symlinks=False)


#
# Path helpers
#


def copy_profdata(indir: Path, outdir: Path) -> None:
    for p in indir.glob("*.profdata"):
        shutil.copy(p, outdir)


def extend_suffix(path: Path, new_suffix: str) -> Path:
    return path.with_suffix(path.suffix + new_suffix)


def get_prebuilt_binary_paths(
        root: Path,
        toplevel_only: bool = False,
        subdirs: list[str] = ["bin", "lib", "lib64"]) -> list[Path]:

    binary_candidates: list[Path] = []
    for subdir in subdirs:
        if subdir == "bin":
            binary_candidates.extend((root / subdir).iterdir())
        else:
            binary_candidates.extend(
                list((root / subdir).glob("*.so" if toplevel_only else "**/*.so")))
            binary_candidates.extend(
                list((root / subdir).glob("*.so.*" if toplevel_only else "**/*.so.*")))

    return [path for path in binary_candidates if is_elf_file(Path(path))]


def reify_singleton_patterns(base_dir: Path,
                             patterns: list[str],
                             strict: bool = False) -> list[Path]:
    paths: list[Path] = []
    for p in patterns:
        matches = list(base_dir.glob(p))
        if len(matches) == 1:
            paths += matches
        else:
            if len(matches) > 1 or strict:
                raise RuntimeError(
                    f"Unexpected number of matches ({len(matches)}) for pattern: {p}")

    return paths


def set_symlink(src: Path, dst: Path) -> None:
    if not (dst.exists() and dst.is_symlink() and dst.readlink() == src):
        dst.unlink(missing_ok=True)
        os.symlink(src, dst)


#
# Time
#

TIME_US_IN_SECOND = 1e6
TIME_US_IN_MINUTE = 6e7

TIME_MS_IN_SECOND = 1000
TIME_MS_IN_MINUTE = TIME_MS_IN_SECOND * 60
TIME_MS_IN_HOUR = TIME_MS_IN_MINUTE * 60
TIME_MS_IN_DAY = TIME_MS_IN_HOUR * 24


def ms_to_hms(milliseconds: int) -> Tuple[int, int, int, float]:
    seconds = (milliseconds / TIME_MS_IN_SECOND) % 60
    minutes = (milliseconds // TIME_MS_IN_MINUTE) % 60
    hours = (milliseconds // TIME_MS_IN_HOUR) % 24
    days = (milliseconds // TIME_MS_IN_DAY)

    return (days, hours, minutes, seconds)


#
# Templates
#


def instantiate_template_exec(template_path: Path, output_path: Path, **kwargs: Any) -> None:
    instantiate_template_file(template_path, output_path, make_exec=True, **kwargs)
    output_path.chmod(output_path.stat().st_mode | stat.S_IEXEC)


def instantiate_template_file(template_path: Path, output_path: Path, **kwargs: Any) -> None:
    with open(output_path, "w") as output_file:
        output_file.write(instantiate_template(template_path, **kwargs))


def instantiate_template(template_path: Path, **kwargs: Any) -> str:
    with open(template_path) as template_file:
        template = Template(template_file.read())
    return template.substitute(**kwargs)


#
# Archive Helpers
#


def archive_create(
        out_path_stem: Path,
        root_dir: Path,
        base_dir: Path = Path("."),
        overwrite: bool = False) -> None:
    archive_path = out_path_stem.with_suffix(out_path_stem.suffix + ".tar")

    if overwrite:
        archive_path.unlink(missing_ok=True)

        compressed_path = out_path_stem.with_suffix(out_path_stem.suffix + ".tar.xz")
        compressed_path.unlink(missing_ok=True)

    if base_dir.is_absolute():
        base_dir = base_dir.relative_to(root_dir)

    if build_platform.is_windows():
        shutil.make_archive(str(out_path_stem), "xztar", str(root_dir), str(base_dir))

    else:
        # Archive, but don't compress; we want to make sure we don't use the system xz.
        subprocess.run(
            [
                BIN_PATH_TAR,
                "-C",
                root_dir,
                "-cf",
                archive_path,
                base_dir,
            ],
            cwd=root_dir,
            check=True) # yapf: disable

        # Compress
        compress_file(archive_path)


def archive_extract(archive_path: Path, extract_dir: Path, strip_components: int = 0) -> None:

    def do_strip_components() -> None:
        for _ in range(strip_components):
            for child in list(extract_dir.iterdir()):
                if child.is_dir():
                    for grandchild in list(child.iterdir()):
                        grandchild.rename(extract_dir / grandchild.name)

                    child.rmdir()

    if build_platform.is_windows():
        shutil.unpack_archive(str(archive_path), str(extract_dir))
        do_strip_components()

    else:
        if archive_path.suffix == ".zip":
            subprocess.run([LINK_PATH_UNZIP, "-d", extract_dir, archive_path],
                           cwd=extract_dir,
                           check=True)
            do_strip_components()

        elif archive_path.name.endswith(".tar.xz"):
            with tempfile.NamedTemporaryFile(prefix=archive_path.stem) as temp_file:
                decompress_file(archive_path, Path(temp_file.name))
                command: list[str] = [
                    BIN_PATH_TAR.as_posix(),
                    "-C",
                    extract_dir.as_posix(),
                    "-xf",
                    temp_file.name,
                ]
                if strip_components > 0:
                    command.append(f"--strip-components={strip_components}")

                subprocess.run(command, cwd=extract_dir, check=True)

        else:
            raise ScriptException(f"Unknown archive type: {archive_path.name}")


def is_archive(test_path: Path) -> bool:
    return test_path.name.endswith(".zip") or test_path.name.endswith(".tar.xz")


def compress_file(file_path: Path) -> None:
    subprocess.run(
        [
            BIN_PATH_XZ,
            "--compress",
            "--threads=0", # Tell xz to use as many threads as it feels is appropriate
            file_path,
        ],
        check=True)


def decompress_file(file_path: Path, dest_path: Path) -> None:
    if file_path.suffix == ".xz":
        with open(dest_path, "w+b") as out_file:
            subprocess.run(
                [
                    BIN_PATH_XZ,
                    "--decompress",
                    "--keep",
                    "--threads=0", # Tell xz to use as many threads as it feels is appropriate
                    "--stdout",
                    file_path,
                ],
                stdout=out_file,
                check=True)
    else:
        raise ScriptException(f"Unsupported archive type: {file_path}")


#
# Debugging Helpers
#


def print_context() -> None:
    print()
    print(f"Current working directory: {str(Path.cwd())}")

    print()
    print("Environment Variables:")
    for key, val in sorted(os.environ.items()):
        print(f"\t{key} => {val}")
    print()

    prebuilt_cache = OUT_PATH / "prebuilt_cached"
    if prebuilt_cache.exists():
        print()
        print(f"Contents of {str(prebuilt_cache)} :")
        for (dirpath, _, filenames) in os.walk(prebuilt_cache):
            for file_name in filenames:
                print(f"\t{os.path.join(dirpath, file_name)}")


def print_fs_tree(path: Path) -> None:
    for (dirpath, _, filenames) in os.walk(path):
        for file_name in filenames:
            print(os.path.join(dirpath, file_name))

    print()


#
# Miscellaneous Helpers
#


def dig(root: dict[Any, Any], *keys: Any, default: Any = None) -> Any:
    curr_item = root
    for key in keys:
        if type(curr_item) == dict and key in curr_item:
            curr_item = curr_item[key]
        else:
            return default

    return curr_item
