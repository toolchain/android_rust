# Copyright (C) 2023 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from pathlib import Path
import shutil

from android_rust.paths import CARGO_DENY_CONFIG_PATH, OUT_PATH
from android_rust.toolchains import RUST_TOOLCHAIN_HOST
from android_rust.utils import run_and_exit_on_failure


def remove_cargo_from_path(env: dict[str, str]) -> None:
    which_cargo = shutil.which("cargo")
    if which_cargo is not None:
        cargo_path = Path(which_cargo)
        env["PATH"] = os.pathsep.join(
            [p for p in env["PATH"].split(os.pathsep) if p != str(cargo_path.parent)])


class Crate:

    def __init__(
        self,
        crate_path: Path,
        env: dict[str, str],
        cargo_path: Path = RUST_TOOLCHAIN_HOST.cargo(),
        target: str | None = None,
        linker: Path | None = None,
    ) -> None:
        self.crate_path = crate_path
        self.env = env.copy()
        # TODO: Use tomllib to get the crate name. But this requires python 3.11
        self.build_dir = OUT_PATH / crate_path.name
        self.cargo_path = cargo_path
        # Make sure that cargo is in our path, so plugins, rustc, etc. are run from there.
        self.env["PATH"] = os.pathsep.join(
            [str(self.cargo_path.resolve().parent), self.env["PATH"]])
        # as_posix() is necessary here to prevent the backslashes from being
        # interpreted as the start of an esacape sequence.
        self.common_command_prefix = [
            self.cargo_path,
            "--config",
            'source.crates-io.replace-with="vendored-sources"',
            "--config",
            'source.vendored-sources.directory="android/vendor"',
            "--config",
            f'build.target-dir="{self.build_dir.as_posix()}"',
        ]
        if target and linker:
            self.common_command_prefix.extend(
                ["--config", f'target.{target}.linker="{linker.as_posix()}"'])
            self.common_command_prefix.extend(["--config", f'build.target="{target}"'])
            self.target = target

    def cargo_deny(self) -> None:
        print("Running cargo deny")
        run_and_exit_on_failure(
            self.common_command_prefix + [
                "--frozen",
                "--locked",
                "deny",
                "check",
                "--config",
                CARGO_DENY_CONFIG_PATH, # For now, don't check security advisories
                "licenses",
                "bans",
                "sources",
            ],
            "Failed to run 'cargo deny'",
            cwd=self.crate_path,
            env=self.env,
        )

    def cargo_vet(self) -> None:
        print("Running cargo vet")
        run_and_exit_on_failure(
            self.common_command_prefix + [
                "--frozen",
                "--locked",
                "vet",
                "--store-path",
                "android/supply-chain",
            ],
            "Failed to run 'cargo vet'",
            cwd=self.crate_path,
            env=self.env,
        )

    def cargo_vet_init(self) -> None:
        print("Initializing cargo vet")
        run_and_exit_on_failure(
            self.common_command_prefix + [
                "--frozen",
                "--locked",
                "vet",
                "init",
                "--store-path",
                "android/supply-chain",
            ],
            "Failed to run 'cargo vet init'",
            cwd=self.crate_path,
            env=self.env,
        )
        run_and_exit_on_failure(
            self.common_command_prefix + [
                "--frozen",
                "--locked",
                "vet",
                "import",
                "google",
                "--store-path",
                "android/supply-chain",
            ],
            "Failed to run 'cargo vet import google'",
            cwd=self.crate_path,
            env=self.env,
        )

    def cargo_vet_prune(self) -> None:
        run_and_exit_on_failure(
            self.common_command_prefix + [
                "--frozen",
                "--locked",
                "vet",
                "prune",
                "--store-path",
                "android/supply-chain",
            ],
            "Failed to run 'cargo vet prune'",
            cwd=self.crate_path,
            env=self.env,
        )

    def cargo_build(self) -> None:
        print("Running cargo build")
        run_and_exit_on_failure(
            self.common_command_prefix + [
                "--offline",
                "build",
                "--release",
            ],
            "Failed to run 'cargo build'",
            cwd=self.crate_path,
            env=self.env,
        )

    def cargo_test(self) -> None:
        print("Running cargo test")
        run_and_exit_on_failure(
            self.common_command_prefix + [
                "--offline",
                "test",
                "--release",
            ],
            "Failed to run 'cargo test'",
            cwd=self.crate_path,
            env=self.env,
        )

    def cargo_vendor(self, versioned_dirs: bool = True) -> None:
        print("Running cargo vendor")
        run_and_exit_on_failure(
            [self.cargo_path, "vendor", "android/vendor"] +
            (["--versioned-dirs"] if versioned_dirs else []),
            "Failed to run 'cargo vendor'",
            cwd=self.crate_path,
            env=self.env,
        )

    def cargo_install(self, root: Path | str) -> None:
        run_and_exit_on_failure(
            self.common_command_prefix + [
                "--offline",
                "install",
                "--target",
                self.target,
                "--path",
                ".",
                "--root",
                root,
            ],
            "Failed to run 'cargo install'",
            cwd=self.crate_path,
            env=self.env,
        )
