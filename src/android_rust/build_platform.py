#
# Copyright (C) 2019 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""build_platform provides various ways of naming the environment we are
building under for the purpose of selecting the correct paths and targets.
"""

from enum import StrEnum
import platform


class PlatformType(StrEnum):
    DARWIN = "darwin"
    LINUX = "linux"
    WINDOWS = "windows"


def system() -> PlatformType:
    """Returns a canonicalized OS type of the machine executing the script.
    Will be one of 'linux', 'darwin', or 'windows'."""
    match platform.system():
        case "Darwin":
            return PlatformType.DARWIN
        case "Linux":
            return PlatformType.LINUX
        case "Windows":
            return PlatformType.WINDOWS
        case sys:
            raise RuntimeError(f"Unknown System: {sys}")


def is_linux() -> bool:
    return platform.system() == "Linux"


def is_darwin() -> bool:
    return platform.system() == "Darwin"


def is_windows() -> bool:
    return platform.system() == "Windows"


def prebuilt() -> str:
    """Returns the prebuilt subdirectory for prebuilts which do not use
    subarch specialization."""
    return system().name.lower() + "-x86"


def prebuilt_full() -> str:
    """Returns the prebuilt subdirectory for prebuilts which have subarch
    specialization available.
    """
    return system().name.lower() + "-x86_64"


def triple() -> str:
    """Returns the target triple of the build environment."""
    match system():
        case PlatformType.LINUX:
            return "x86_64-unknown-linux-gnu"
        case PlatformType.DARWIN:
            return "x86_64-apple-darwin"
        case PlatformType.WINDOWS:
            return "x86_64-pc-windows-gnu"
        case _:
            raise RuntimeError("Unreachable")


def rpath_origin() -> str:
    """Returns the string used to indicate the root loader context"""
    if is_darwin():
        return "@loader_path"
    else:
        return "$ORIGIN"
