# Copyright (C) 2024 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Command definitions for Android Rust toolchain builds"""

import os

from android_rust.paths import RUST_PREBUILT_PATH, RUST_STAGE0_VERSION

#
# Target Definitions
#

BUILD_ID = os.environ.get("BUILD_NUMBER", "dev")

# yapf: disable
TARGET_DEFS: dict[str, dict[str, list[str]]] = {
    "git_main-rust-testing": {
        "pixel-trunk_staging-userdebug": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/rust-linux/",
            "--target", "cheetah",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--all-rust",
        ],
    },

    "main-plus-rust": {
        "aosp_cf_arm64_phone-trunk_staging-userdebug": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/rust-linux/",
            "--target", "aosp_cf_arm64_phone",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--all-rust",
            "--image",
        ],
        "aosp_cf_riscv64_phone-trunk_staging-userdebug": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/rust-linux/",
            "--target", "aosp_cf_riscv64_phone",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--image",
        ],
        "aosp_cf_x86_64_phone-trunk_staging-userdebug": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/rust-linux/",
            "--target", "aosp_cf_x86_64_phone",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--all-rust",
            "--image",
            "--test-targets",
        ],
        "aosp_cf_x86_64_phone-trunk_staging-user": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/rust-linux/",
            "--target", "aosp_cf_x86_64_phone",
            "--release", "trunk_staging",
            "--variant", "user",
            "--all-rust",
        ],
        "aosp_cf_x86_64_phone-trunk_staging-eng": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/rust-linux/",
            "--target", "aosp_cf_x86_64_phone",
            "--release", "trunk_staging",
            "--variant", "eng",
            "--all-rust",
        ],
        "aosp_pixel-trunk_staging-userdebug": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/rust-linux/",
            "--target", "aosp_husky",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--all-rust",
            "--image",
        ],
        "bolt-instrument": [
            "toolchain/android_rust/tools/boltifyer.py",
            "--profile-generate-relative",
            "--",
            "toolchain/android_rust/artifacts/testing/rust-1.78.0-pgo-opt-relocs.tar.xz",
        ],
        "bolt-optimize": [
            "toolchain/android_rust/tools/boltifyer.py",
            "--profile-use", "toolchain/android_rust/artifacts/testing/bolt-profiles-merged.tar.xz",
            "toolchain/android_rust/artifacts/testing/rust-1.78.0-pgo-opt-relocs.tar.xz",
        ],
        "merge_profiles-bolt": [
            "toolchain/android_rust/tools/merge_profiles.py",
            "toolchain/android_rust/artifacts/testing/bolt-profiles-cf_x86_64_phone.tar.xz",
            "toolchain/android_rust/artifacts/testing/bolt-profiles-raven.tar.xz",
        ],
        "merge_profiles-pgo": [
            "toolchain/android_rust/tools/merge_profiles.py",
            "toolchain/android_rust/artifacts/testing/pgo/toolchain",
            "toolchain/android_rust/artifacts/testing/pgo/cf_x86_64_phone",
            "toolchain/android_rust/artifacts/testing/pgo/raven",
        ],
        "rust-linux": [
            "toolchain/android_rust/tools/build.py",
            "--build-name", f"{BUILD_ID}-linux-x86",
            "--ndk", "/ndk",
            "--clang-prebuilt", f"out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--llvm-prebuilt", f"out/prebuilt_cached/llvm/linux_builders/clang-*-linux-x86-builders.tar.xz",
            "--llvm-version", "19",
            "--llvm-linkage", "static",
            "--lto", "thin",
        ],
        "rust-linux_ndk": [
            "toolchain/android_rust/tools/build.py",
            "--build-name", f"ndk-{BUILD_ID}-linux-x86",
            "--ndk", "out/prebuilt_cached/android-ndk-*-linux-x86_64.zip",
            "--clang-prebuilt", f"out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--llvm-prebuilt", f"out/prebuilt_cached/llvm/linux_builders/clang-*-linux-x86-builders.tar.xz",
            "--llvm-version", "19",
            "--llvm-linkage", "static",
            "--lto", "thin",
            "--no-bare-targets",
            "--no-host-multilibs",
        ],
        "rustdoc": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/rust-linux/",
            "--target", "aosp_x86_64",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--rustdoc",
        ],
    },

    "rust-profiling": {
        # Stage 1

        "stage1-llvm-pgo-instrument": [
            "toolchain/llvm_android/build.py",
            "--build-name", f"pgo-instrumented-{BUILD_ID}",
            # TODO (b/376114796): Resume using bootstrap build when bug is resolved
            # "--bootstrap-use", f"out/prebuilt_cached/llvm/linux_bootstrap/stage1-install.tar.xz",
            "--no-build", "windows,lldb",
            "--skip-tests",
            "--builders-package",
            "--create-tar",
            "--build-instrumented",
        ],

        "stage1-rust-pgo-instrument": [
            "toolchain/android_rust/tools/build.py",
            "--build-name", f"pgo-instrumented-{BUILD_ID}-linux-x86",
            "--ndk", "/ndk",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--llvm-prebuilt", f"out/prebuilt_cached/stage1-llvm-pgo-instrument/clang-pgo-instrumented-{BUILD_ID}-linux-x86-builders.tar.xz",
            "--llvm-version", "19",
            "--llvm-linkage", "static",
            "--cgu1",
            "--profile-generate",
        ],

        # Stage 2

        "stage2-cf_arm64_phone": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/stage1-rust-pgo-instrument/rust-pgo-instrumented-{BUILD_ID}-linux-x86.tar.xz",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--target", "aosp_cf_arm64_phone",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--image",
            "--profile-generate",
        ],
        "stage2-cf_riscv64_phone": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/stage1-rust-pgo-instrument/rust-pgo-instrumented-{BUILD_ID}-linux-x86.tar.xz",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--target", "aosp_cf_riscv64_phone",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--image",
            "--profile-generate",
        ],
        "stage2-cf_x86_64_phone": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/stage1-rust-pgo-instrument/rust-pgo-instrumented-{BUILD_ID}-linux-x86.tar.xz",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--target", "aosp_cf_x86_64_phone",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--image",
            "--profile-generate",
        ],
        "stage2-merge": [
            "toolchain/android_rust/tools/merge_profiles.py",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "out/prebuilt_cached/stage1-rust-pgo-instrument",
            "out/prebuilt_cached/stage2-cf_arm64_phone",
            "out/prebuilt_cached/stage2-cf_riscv64_phone",
            "out/prebuilt_cached/stage2-cf_x86_64_phone",
        ],

        # Stage 3

         "stage3-llvm-pgo-optimize": [
            "toolchain/llvm_android/build.py",
            "--build-name", f"pgo-optimized-{BUILD_ID}",
            "--bootstrap-use", "out/prebuilt_cached/llvm/linux_bootstrap/stage1-install.tar.xz",
            "--no-build", "windows,lldb",
            "--skip-tests",
            "--builders-package",
            "--create-tar",
            "--pgo", "out/prebuilt_cached/stage2-merge/rust.profdata",
        ],

        "stage3-rust-pgo-optimize": [
            "toolchain/android_rust/tools/build.py",
            "--build-name", f"pgo-optimized-{BUILD_ID}-linux-x86",
            "--ndk", "/ndk",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--llvm-prebuilt", f"out/prebuilt_cached/stage3-llvm-pgo-optimize/clang-pgo-optimized-{BUILD_ID}-linux-x86-builders.tar.xz",
            "--llvm-version", "19",
            "--llvm-linkage", "static",
            "--lto", "thin",
            "--cgu1",
            "--emit-relocs",
            "--profile-use", "out/prebuilt_cached/stage2-merge/",
        ],

        # Stage 4

        "stage4-bolt-instrument": [
            "toolchain/android_rust/tools/boltifyer.py",
            "--build-name", f"bolt-instrumented-{BUILD_ID}-linux-x86",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--profile-generate-relative",
            "--",
            f"out/prebuilt_cached/stage3-rust-pgo-optimize/rust-pgo-optimized-{BUILD_ID}-linux-x86.tar.xz",
        ],

        # Stage 5

        "stage5-cf_arm64_phone": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/stage4-bolt-instrument/rust-bolt-instrumented-{BUILD_ID}-linux-x86.tar.xz",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--target", "aosp_cf_arm64_phone",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--image",
            "--bolt-profile-generate",
        ],
        "stage5-cf_riscv64_phone": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/stage4-bolt-instrument/rust-bolt-instrumented-{BUILD_ID}-linux-x86.tar.xz",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--target", "aosp_cf_riscv64_phone",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--image",
            "--bolt-profile-generate",
        ],
        "stage5-cf_x86_64_phone": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/stage4-bolt-instrument/rust-bolt-instrumented-{BUILD_ID}-linux-x86.tar.xz",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--target", "aosp_cf_x86_64_phone",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--image",
            "--bolt-profile-generate",
        ],
        "stage5-merge": [
            "toolchain/android_rust/tools/merge_profiles.py",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "out/prebuilt_cached/stage5-cf_arm64_phone/bolt-profiles.tar.xz",
            "out/prebuilt_cached/stage5-cf_riscv64_phone/bolt-profiles.tar.xz",
            "out/prebuilt_cached/stage5-cf_x86_64_phone/bolt-profiles.tar.xz",
        ],

        # Stage 6

        "stage6-bolt-optimize": [
            "toolchain/android_rust/tools/boltifyer.py",
            "--build-name", f"bolt-optimized-{BUILD_ID}-linux-x86",
            "--clang-prebuilt", "out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--profile-use", f"out/prebuilt_cached/stage5-merge/bolt-profiles.tar.xz",
            f"out/prebuilt_cached/stage3-rust-pgo-optimize/rust-pgo-optimized-{BUILD_ID}-linux-x86.tar.xz",
        ],

        # Stage 7

        "stage7-cf_x86_64_phone": [
            "toolchain/android_rust/tools/test_compiler.py",
            "--prebuilt-path", f"out/prebuilt_cached/stage6-bolt-optimize/rust-bolt-optimized-{BUILD_ID}-linux-x86.tar.xz",
            "--target", "aosp_cf_x86_64_phone",
            "--release", "trunk_staging",
            "--variant", "userdebug",
            "--image",
        ],
    },

    "rust-toolchain": {
        "llvm-linux_pgo": [
            "toolchain/llvm_android/build.py",
            "--build-name", f"pgo-optimized-{BUILD_ID}",
            "--bootstrap-use", "out/prebuilt_cached/llvm/linux_bootstrap/stage1-install.tar.xz",
            "--no-build", "windows,lldb",
            "--skip-tests",
            "--builders-package",
            "--create-tar",
            "--pgo", "out/prebuilt_cached/stage2-merge/rust.profdata",
        ],
        "rust-darwin_mac": [
            "toolchain/android_rust/tools/build.py",
            "--build-name", f"{BUILD_ID}-darwin-x86",
            "--ndk-search-path", "out/prebuilt_cached/",
            # "--unified-prebuilt", f"out/prebuilt_cached/llvm/darwin_mac/clang-*-darwin-x86-builders.tar.xz",
            # "--llvm-version", "19",
            # "--llvm-linkage", "static",
            "--llvm-linkage", "shared",
        ],
        "rust-linux_glibc": [
            "toolchain/android_rust/tools/build.py",
            "--build-name", f"{BUILD_ID}-linux-x86",
            "--ndk", "/ndk",
            "--clang-prebuilt", f"out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--llvm-prebuilt", f"out/prebuilt_cached/llvm/linux_builders/clang-*-linux-x86-builders.tar.xz",
            "--llvm-version", "19",
            "--llvm-linkage", "static",
            "--lto", "thin",
            "--cgu1",
        ],
        "rust-linux_musl": [
            "toolchain/android_rust/tools/build.py",
            "--build-name", f"{BUILD_ID}-linux_musl-x86",
            "--host", "x86_64-unknown-linux-musl",
            "--ndk", "/ndk",
            "--unified-prebuilt", f"out/prebuilt_cached/llvm/linux_musl/clang-*-linux_musl-x86-builders.tar.xz",
            "--llvm-version", "19",
            "--llvm-linkage", "static",
            "--rust-prebuilt", (RUST_PREBUILT_PATH / "linux-musl-x86" / RUST_STAGE0_VERSION).as_posix(),
            "--rust-stage0-triple", "x86_64-unknown-linux-musl"
        ],
        "rust-linux_pgo_bolt": [
            "toolchain/android_rust/tools/build.py",
            "--build-name", f"{BUILD_ID}-linux-x86",
            "--bolt-name", f"bolt-{BUILD_ID}-linux-x86",
            "--ndk", "/ndk",
            "--clang-prebuilt", f"out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
            "--llvm-prebuilt", f"out/prebuilt_cached/llvm-linux_pgo/clang-pgo-optimized-{BUILD_ID}-linux-x86-builders.tar.xz",
            "--llvm-version", "19",
            "--llvm-linkage", "static",
            "--lto", "thin",
            "--cgu1",
            "--emit-relocs",
            "--profile-use", "out/prebuilt_cached/stage2-merge",
            "--bolt-profile-use", "out/prebuilt_cached/stage5-merge/bolt-profiles.tar.xz",
        ],
        # "rust-windows_gnu": [
        #     "toolchain/android_rust/tools/build.py",
        #     "--build-name", f"cross-{BUILD_ID}-windows_gnu-x86",
        #     "--host", "x86_64-pc-windows-gnu",
        #     "--ndk-search-path", "out/prebuilt_cached/",
        #     "--clang-prebuilt", f"out/prebuilt_cached/llvm/linux/clang-*-linux-x86.tar.xz",
        #     "--llvm-prebuilt", f"out/prebuilt_cached/llvm/windows_x86_64/clang-*-window-x86-builders.tar.xz",
        #     "--llvm-version", "19",
        #     "--llvm-linkage", "static",
        # ],
        "rust-windows_gnu_native": [
            "toolchain\\android_rust\\tools\\build.py",
            "--build-name", f"{BUILD_ID}-windows-x86",
            "--ndk-search-path", "out\\prebuilt_cached",
            # "--unified-prebuilt", f"out\\prebuilt_cached\\llvm\\windows_x86_64\\clang-*-windows-x86-builders.tar.xz",
            # "--llvm-version", "19",
            "--llvm-linkage", "static",
        ],
    },

    "test": {
        "rust-linux": [
            "toolchain/android_rust/tools/build.py",
            "--build-name", f"linux-glibc-{BUILD_ID}",
            "--llvm-linkage", "shared",
            "--lto", "thin",
            "--ndk", "/ndk",
            "--host-only",
        ],
        "help": [
            "toolchain/android_rust/tools/build.py",
            "--help",
        ]
    }
}
# yapf: enable