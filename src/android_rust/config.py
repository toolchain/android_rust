# Copyright (C) 2019 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Handles generation of config.toml for the rustc build."""

import argparse
import os
from pathlib import Path
import subprocess
from typing import Any, Tuple, Union

import android_rust.build_platform as build_platform
from android_rust.paths import *
from android_rust.toolchains import CLANG_TOOLCHAIN_LINUX, CLANG_TOOLCHAIN_WINDOWS
from android_rust.utils import ScriptException, instantiate_template, instantiate_template_file, instantiate_template_exec

#
# Constants
#

HOST_TARGETS_DEFAULT: list[str] = [
    "x86_64-unknown-linux-gnu",
    "i686-unknown-linux-gnu",
    "x86_64-unknown-linux-musl",
    "i686-unknown-linux-musl",
    "x86_64-pc-windows-gnu",
]

BARE_TARGETS_DEFAULT: list[str] = [
    "aarch64-unknown-none",
    "armv7a-none-eabi",
    "riscv32i-unknown-none-elf",
    "riscv32imc-unknown-none-elf",

 # Added for b/301288222
    "x86_64-unknown-uefi",
    "i686-unknown-uefi",
    "aarch64-unknown-uefi",
]

DEVICE_TARGET_AND_ARCH: dict[str, str] = {
    "aarch64-linux-android": "aarch64",
    "armv7-linux-androideabi": "arm",
    "x86_64-linux-android": "x86_64",
    "i686-linux-android": "i386",
    "riscv64-linux-android": "riscv64",
}

# rustc replaces these triples with other targets when invoking clang.  To
# avoid confusion we won't emit `--target=<triple>` flags into the compiler and
# linker wrappers.
RUSTC_TRANSLATED_TRIPLES: list[str] = [
    "x86_64-unknown-uefi",
    "i686-unknown-uefi",
    "aarch64-unknown-uefi",
]

DEVICE_TARGETS_DEFAULT: list[str] = list(DEVICE_TARGET_AND_ARCH.keys())

ANDROID_TARGET_VERSION: str = "35"

# yapf: disable
_CMD = ".cmd" if build_platform.is_windows() else ""
BARE_CC_WRAPPER_TEMPLATE:       Path = TEMPLATES_PATH / f"bare_cc_wrapper{_CMD}.template"
BARE_LINKER_WRAPPER_TEMPLATE:   Path = TEMPLATES_PATH / f"bare_linker_wrapper{_CMD}.template"
BARE_TARGET_TEMPLATE:           Path = TEMPLATES_PATH / "bare_target.template"
CONFIG_TOML_TEMPLATE:           Path = TEMPLATES_PATH / "config.toml.template"
DEVICE_CC_WRAPPER_TEMPLATE:     Path = TEMPLATES_PATH / f"device_cc_wrapper{_CMD}.template"
DEVICE_LINKER_WRAPPER_TEMPLATE: Path = TEMPLATES_PATH / f"device_linker_wrapper{_CMD}.template"
DEVICE_TARGET_TEMPLATE:         Path = TEMPLATES_PATH / "device_target.template"
HOST_CC_WRAPPER_TEMPLATE:       Path = TEMPLATES_PATH / f"host_cc_wrapper{_CMD}.template"
HOST_CXX_WRAPPER_TEMPLATE:      Path = TEMPLATES_PATH / f"host_cxx_wrapper{_CMD}.template"
HOST_LINKER_WRAPPER_TEMPLATE:   Path = TEMPLATES_PATH / f"host_linker_wrapper{_CMD}.template"
HOST_TARGET_TEMPLATE:           Path = TEMPLATES_PATH / "host_target.template"
# yapf: enable

#
# Helper functions
#


def populate_target_rustflags(
        target_rustflags: dict[str, list[str]], targets: list[str], flags: list[str]) -> None:
    for target in targets:
        if target in target_rustflags:
            target_rustflags[target] += flags
        else:
            target_rustflags[target] = flags


def vp_counters_flags(num_counters: int) -> str:
    return f"-mllvm -vp-counters-per-site={num_counters}"


def apply_target_flags(
        target: str,
        cc_flags: list[str],
        cxx_flags: list[str],
        ld_flags: list[str],
        android_version: str = "",
        escape: bool = True) -> Tuple[str, str, str]:
    if target in RUSTC_TRANSLATED_TRIPLES:
        target_flag = []
    else:
        target_flag = [f"--target={target}{android_version}"]

    cc_flags_str = " ".join(
        target_flag + TARGET_COMMON_FLAGS.get(target, []) + TARGET_CC_FLAGS.get(target, []) +
        cc_flags)

    cxx_flags_str = " ".join(
        target_flag + TARGET_COMMON_FLAGS.get(target, []) + TARGET_CC_FLAGS.get(target, []) +
        TARGET_CXX_FLAGS.get(target, []) + cxx_flags)

    ld_flags_str = " ".join(
        target_flag + TARGET_COMMON_FLAGS.get(target, []) + TARGET_LD_FLAGS.get(target, []) +
        ld_flags)

    if escape:
        cc_flags_str = cc_flags_str.replace("$", "\\$")
        cxx_flags_str = cxx_flags_str.replace("$", "\\$")
        ld_flags_str = ld_flags_str.replace("$", "\\$")

    return (cc_flags_str, cxx_flags_str, ld_flags_str)


def darwin_sysroot() -> str:
    if build_platform.is_darwin():
        # Apple removed the normal sysroot at / on Mojave+, so we need
        # to go hunt for it on OSX
        # On pre-Mojave, this command will output the empty string.
        output = subprocess.check_output(["xcrun", "--sdk", "macosx", "--show-sdk-path"])
        return output.rstrip().decode("utf-8")
    return ""


def print_flags(target: str, cc_flags: str, cxx_flags: str, ld_flags: str) -> None:
    print(f"Compiler/linker flags for {target}")
    print(f"CC  Flags: {cc_flags}")
    if cxx_flags:
        print(f"CXX Flags: {cxx_flags}")
    print(f"LD  Flags: {ld_flags}\n")


def get_llvm_config(args: argparse.Namespace) -> str:
    if args.llvm_prebuilt is None:
        return ""
    else:
        _exe = ".exe" if build_platform.is_windows() else ""
        llvm_config = f"llvm-config = \"{args.llvm_prebuilt.base_dir.as_posix()}/bin/llvm-config{_exe}\"\n"
        llvm_config += f"llvm-has-rust-patches = {str(args.llvm_prebuilt_has_rust_patches).lower()}"

        return llvm_config


def get_wrapper_paths(target: str) -> Tuple[Path, Path, Path]:
    return (
        OUT_PATH_WRAPPERS / f"clang-{target}{_CMD}",
        OUT_PATH_WRAPPERS / f"clang++-{target}{_CMD}",
        OUT_PATH_WRAPPERS / f"linker-{target}{_CMD}",
    )


#
# Target configuration
#


def host_config(
        target: str,
        cc_flags: list[str],
        cxx_flags: list[str],
        ld_flags: list[str],
        env: dict[str, str],
        args: argparse.Namespace) -> str:
    cc_wrapper_name, cxx_wrapper_name, ld_wrapper_name = get_wrapper_paths(target)
    cc_flags_str, cxx_flags_str, ld_flags_str = apply_target_flags(target, cc_flags, cxx_flags, ld_flags)
    print_flags(target, cc_flags_str, cxx_flags_str, ld_flags_str)

    envified_target = target.replace("-", "_")
    env[f"CC_{envified_target}"] = str(cc_wrapper_name)
    env[f"CXX_{envified_target}"] = str(cxx_wrapper_name)
    env[f"LD_{envified_target}"] = str(ld_wrapper_name)
    env[f"RANLIB_{envified_target}"] = str(args.clang_prebuilt.ranlib())

    if target == build_platform.triple():
        env["CC"] = str(cc_wrapper_name)
        env["LD"] = str(ld_wrapper_name)

    profiler = "false"
    if args.llvm_prebuilt is None:
        if "linux" in target:
            profiler = "true"

    else:
        if "linux" in target:
            runtimes_path = args.llvm_prebuilt.runtimes_path(target)
            if runtimes_path.exists():
                profiler = f"\"{runtimes_path}/libclang_rt.profile.a\""

    if "musl" in target:
        musl_config = f"musl-root = \"{HOST_SYSROOTS[target]}\"\n"
        musl_config += f"musl-libdir = \"{HOST_SYSROOTS[target]}/lib\""
    else:
        musl_config = ""

    instantiate_template_exec(
        HOST_CC_WRAPPER_TEMPLATE,
        cc_wrapper_name,
        real_cc=args.clang_prebuilt.clang(),
        cc_flags=cc_flags_str)

    instantiate_template_exec(
        HOST_CXX_WRAPPER_TEMPLATE,
        cxx_wrapper_name,
        real_cxx=args.clang_prebuilt.clangxx(),
        cxx_flags=cxx_flags_str)

    instantiate_template_exec(
        HOST_LINKER_WRAPPER_TEMPLATE,
        ld_wrapper_name,
        real_cc=args.clang_prebuilt.clang(),
        ld_flags=ld_flags_str)

    # as_posix() is necessary here to prevent the backslashes from being
    # interpreted as the start of an escape sequence on Windows hosts.
    return instantiate_template(
        HOST_TARGET_TEMPLATE,
        target=target,
        cc=cc_wrapper_name.as_posix(),
        cxx=cxx_wrapper_name.as_posix(),
        linker=ld_wrapper_name.as_posix(),
        ar=args.clang_prebuilt.ar().as_posix(),
        ranlib=args.clang_prebuilt.ranlib().as_posix(),
        profiler=profiler,
        musl_config=musl_config,
        llvm_config=get_llvm_config(args))


def bare_config(
        target: str,
        cc_flags: list[str],
        ld_flags: list[str],
        env: dict[str, str],
        args: argparse.Namespace) -> str:
    cc_wrapper_name, _, ld_wrapper_name = get_wrapper_paths(target)
    cc_flags_str, _, ld_flags_str = apply_target_flags(target, cc_flags, [], ld_flags)
    print_flags(target, cc_flags_str, "", ld_flags_str)

    envified_target = target.replace("-", "_")
    env[f"CC_{envified_target}"] = str(cc_wrapper_name)
    env[f"LD_{envified_target}"] = str(ld_wrapper_name)

    instantiate_template_exec(
        BARE_CC_WRAPPER_TEMPLATE,
        cc_wrapper_name,
        real_cc=args.clang_prebuilt.clang(),
        cc_flags=cc_flags_str)

    instantiate_template_exec(
        BARE_LINKER_WRAPPER_TEMPLATE,
        ld_wrapper_name,
        real_cc=args.clang_prebuilt.clang(),
        ld_flags=ld_flags_str)

    # as_posix() is necessary here to prevent the backslashes from being
    # interpreted as the start of an escape sequence on Windows hosts.
    return instantiate_template(
        BARE_TARGET_TEMPLATE,
        target=target,
        cc=cc_wrapper_name.as_posix(),
        cxx=cc_wrapper_name.as_posix(),
        linker=ld_wrapper_name.as_posix(),
        ar=args.clang_prebuilt.ar().as_posix(),
        llvm_config=get_llvm_config(args))


def device_config(
        target: str,
        cc_flags: list[str],
        ld_flags: list[str],
        env: dict[str, str],
        args: argparse.Namespace) -> str:
    cc_wrapper_name, _, ld_wrapper_name = get_wrapper_paths(target)
    cc_flags_str,    _, ld_flags_str    = apply_target_flags(
        target, cc_flags, [], ld_flags, android_version=ANDROID_TARGET_VERSION)
    print_flags(target, cc_flags_str, "", ld_flags_str)

    envified_target = target.replace("-", "_")
    env[f"CC_{envified_target}"] = str(cc_wrapper_name)
    env[f"LD_{envified_target}"] = str(ld_wrapper_name)

    instantiate_template_exec(
        DEVICE_CC_WRAPPER_TEMPLATE,
        cc_wrapper_name,
        real_cc=args.clang_prebuilt.clang(),
        cc_flags=cc_flags_str)

    instantiate_template_exec(
        DEVICE_LINKER_WRAPPER_TEMPLATE,
        ld_wrapper_name,
        real_cc=args.clang_prebuilt.clang(),
        ld_flags=ld_flags_str)

    # as_posix() is necessary here to prevent the backslashes from being
    # interpreted as the start of an escape sequence on Windows hosts.
    return instantiate_template(
        DEVICE_TARGET_TEMPLATE,
        target=target,
        cc=cc_wrapper_name.as_posix(),
        cxx=cc_wrapper_name.as_posix(),
        linker=ld_wrapper_name.as_posix(),
        ar=args.clang_prebuilt.ar().as_posix(),
        llvm_config=get_llvm_config(args))


#
# Main configuration
#

# yapf: disable
CC_FLAG_PIC:     str = "-fpic"
LD_FLAG_PIC:     str = "-Wl,-mllvm,--relocation-model=pic"
LD_FLAG_USE_LLD: str = "-fuse-ld=lld"
LD_OLD_DTAGS:    str = "-Wl,--disable-new-dtags"
LD_FLAG_RPATH:   str = f"-Wl,-rpath,{build_platform.rpath_origin()}/../lib64"
# yapf: enable

MACOSX_VERSION_FLAG: str = "-mmacosx-version-min=10.14"

HOST_SYSROOTS: dict[str, str] = {
    "x86_64-unknown-linux-gnu": str(GCC_SYSROOT_PATH),
    "i686-unknown-linux-gnu": str(GCC_SYSROOT_PATH),
    "x86_64-apple-darwin": darwin_sysroot(),
    "aarch64-apple-darwin": darwin_sysroot(),
    "x86_64-unknown-linux-musl": str(MUSL_SYSROOT64_PATH),
    "i686-unknown-linux-musl": str(MUSL_SYSROOT32_PATH),
    "x86_64-pc-windows-gnu": str(MINGW_SYSROOT_PATH),
}

TARGET_COMMON_FLAGS: dict[str, list[str]] = {}
for arch, sysroot in HOST_SYSROOTS.items():
    sysroot_flag = f"--sysroot={sysroot}"
    if arch in TARGET_COMMON_FLAGS:
        TARGET_COMMON_FLAGS[arch].append(sysroot_flag)
    else:
        TARGET_COMMON_FLAGS[arch] = [sysroot_flag]

TARGET_CC_FLAGS: dict[str, list[str]] = {
    "x86_64-apple-darwin": ["-D_LIBCPP_AVAILABILITY_HAS_NO_VERBOSE_ABORT=1"],
    "aarch64-apple-darwin": ["-D_LIBCPP_AVAILABILITY_HAS_NO_VERBOSE_ABORT=1"],
    "x86_64-unknown-linux-musl": ["-D_LIBCPP_HAS_MUSL_LIBC"],
    "i686-unknown-linux-musl": ["-D_LIBCPP_HAS_MUSL_LIBC"],
}
TARGET_CXX_FLAGS: dict[str, list[str]] = {}

HOST_LINUX_LD_FLAGS: list[str] = [
    LD_FLAG_USE_LLD,
    "--rtlib=compiler-rt",
    # Rely on rust.llvm-libunwind="in-tree" in config.toml to link host
    # binaries against the in-tree built LLVM libunwind.  The version of
    # libunwind in our prebuilts is for devices only.
    f"-L{GCC_LIB_PATH}",
    LD_OLD_DTAGS,
    LD_FLAG_RPATH,
]
HOST_WINDOWS_LDFLAGS: list[str] = [
    LD_FLAG_USE_LLD, # "--rtlib=compiler-rt",
    f"-B{MINGW_LIBGCC_PATH}",
    f"-L{MINGW_LIBGCC_PATH}",
    f"-L{MINGW_LIB_PATH}",
]
if CLANG_TOOLCHAIN_WINDOWS is not None:
    HOST_WINDOWS_LDFLAGS.append(f"-L{CLANG_TOOLCHAIN_WINDOWS.lib()}")

TARGET_LD_FLAGS: dict[str, list[str]] = {
    # When performing LTO, the LLVM IR generator doesn't know about these
    # target specific symbols. By telling the linker about them ahead of time
    # we avoid an error when they are encountered when the native code is
    # emitted.  See b/201551165 for more information.
    "armv7-linux-androideabi": [
        "-u __aeabi_uidiv",
        "-u __aeabi_idiv0",
        LD_OLD_DTAGS,
        LD_FLAG_RPATH,
    ],
    "x86_64-unknown-linux-gnu":
        HOST_LINUX_LD_FLAGS.copy() + [
            f"-B{GCC_LIBGCC_PATH}",
            f"-L{GCC_LIBGCC_PATH}",
        ],
    "i686-unknown-linux-gnu":
        HOST_LINUX_LD_FLAGS.copy() + [
            f"-B{GCC_LIBGCC_PATH / '32'}",
            f"-L{GCC_LIBGCC_PATH / '32'}",
        ],
    "x86_64-apple-darwin": [LD_FLAG_RPATH,],
    "aarch64-apple-darwin": [LD_FLAG_RPATH,],
    "x86_64-unknown-linux-musl": [
        LD_FLAG_USE_LLD,
        "--rtlib=compiler-rt",
        f"-L{MUSL_SYSROOT64_PATH}/lib",
        f"-B{MUSL_SYSROOT64_PATH}/lib",
        LD_OLD_DTAGS,
        LD_FLAG_RPATH,
    ],
    "i686-unknown-linux-musl": [
        LD_FLAG_USE_LLD,
        "--rtlib=compiler-rt",
        f"-L{MUSL_SYSROOT32_PATH}/lib",
        f"-B{MUSL_SYSROOT32_PATH}/lib",
        LD_OLD_DTAGS,
        LD_FLAG_RPATH,
    ],
    "x86_64-pc-windows-gnu":
        HOST_WINDOWS_LDFLAGS,
}

MUSL_RUSTFLAGS: list[str] = [
    # Rust defaults to using its own crt objects when targeting musl, but we
    # want to use the ones from the sysroot with the embedded dynamic
    # linker.  It defaults to no when targeting glibc, darwin or android.
    "-Clink-self-contained=no",
]

BTI_RUSTFLAGS_X86: list[str] = [
    "-Zcf-protection=branch",
]

BTI_RUSTFLAGS_AARCH64: list[str] = [
    "-Zbranch-protection=bti",
]

TARGET_RUSTFLAGS: dict[str, list[str]] = {
    "x86_64-unknown-linux-musl": MUSL_RUSTFLAGS,
    "i686-unknown-linux-musl": MUSL_RUSTFLAGS,
    "aarch64-unknown-none": BTI_RUSTFLAGS_AARCH64,
    "aarch64-unknown-linux-gnu": BTI_RUSTFLAGS_AARCH64,
    "aarch64-linux-android": BTI_RUSTFLAGS_AARCH64,
    "x86_64-unknown-linux-gnu": BTI_RUSTFLAGS_X86,
}

BARE_RUSTFLAGS: list[str] = [
    "-Cpanic=abort",
]

DEVICE_RUSTFLAGS: list[str] = [
    "-Cpanic=abort",
    "-Copt-level=z",
]

HOST_RUSTFLAGS: list[str] = [
    "-Cpanic=unwind",
]

populate_target_rustflags(TARGET_RUSTFLAGS, BARE_TARGETS_DEFAULT, BARE_RUSTFLAGS)
populate_target_rustflags(TARGET_RUSTFLAGS, DEVICE_TARGETS_DEFAULT, DEVICE_RUSTFLAGS)
populate_target_rustflags(TARGET_RUSTFLAGS, HOST_TARGETS_DEFAULT, HOST_RUSTFLAGS)


def configure(args: argparse.Namespace, env_init: dict[str, str]) -> None:
    """Generates config.toml and compiler wrappers for the rustc build."""
    env_new: dict[str, str] = dict()

    #
    # Update environment variables
    #

    path: list[Union[Path, str]] = [
        BIN_PATH_PYTHON.parent,
        RUST_HOST_STAGE0_PATH / "bin",
        CMAKE_PREBUILT_PATH / "bin",
        NINJA_PREBUILT_PATH,
        BUILD_TOOLS_PREBUILT_MULTICALL,
        BUILD_TOOLS_PREBUILT_BIN_PATH,
        OUT_PATH_BIN_LINKS,
    ]

    # Continue to use the system search paths on Darwin.
    if build_platform.is_darwin():
        path.append(env_init["PATH"])

    if build_platform.is_windows():
        path.extend([
            GIT_USR_BIN_PATH, # So we can find sh.exe
            MINGW_LIB_PATH.parent / "bin",
            MINGW_LIB_PATH.parent / "lib",
        ])

    env_new["PATH"] = os.pathsep.join([str(p) for p in path])

    # System libraries are still required to build the Mac and Windows
    # toolchains.
    if build_platform.is_linux():
        if "LIBRARY_PATH" in env_init:
            del env_init["LIBRARY_PATH"]

    # Use LD_LIBRARY_PATH to tell the build system where to find libc++.so.1
    # without polluting the rpath of the produced artifacts.
    if not build_platform.is_windows():
        env_new["LD_LIBRARY_PATH"] = os.pathsep.join(
            [str(args.clang_prebuilt.lib()), str(args.clang_prebuilt.lib() / args.host)])

        if args.llvm_prebuilt is not None:
            env_new["LD_LIBRARY_PATH"] += f":{(args.llvm_prebuilt.base_dir / 'lib64').as_posix()}"

            if args.llvm_prebuilt != args.clang_prebuilt.base_dir:
                env_new["LD_LIBRARY_PATH"] += f":{(args.llvm_prebuilt.base_dir / 'lib').as_posix()}"

    # Tell the rust bootstrap system where to place its final products
    env_new["DESTDIR"] = str(OUT_PATH_PACKAGE)

    # Tell pkg-config where to find metadata
    env_new["PKG_CONFIG_PATH"] = str(GCC_PKG_CONFIG_PATH)

    # By default, the lzma-sys crate links against the system's liblzma.so.
    # This causes Cargo to propagate a dependency to the system library
    # directory to all of the libraries that depend on lzma-sys.
    #
    # Setting this property causes the crate to build a static liblzma.a and
    # link against that.
    env_new["LZMA_API_STATIC"] = "1"

    # Ensure libz is compiled from in-tree source.
    env_new["LIBZ_SYS_STATIC"] = "1"

    # Explicitly set the host AR path; this is required to build the
    # openssl-src crate.
    env_new["AR"] = str(args.clang_prebuilt.ar())

    env_new["CARGO_HOME"] = str(OUT_PATH_CARGO_HOME)

    #
    # Select the targets
    #

    host_targets: list[str] = [args.host]
    if build_platform.is_darwin():
        host_targets.append('aarch64-apple-darwin')
    if build_platform.is_linux():
        if not args.host_multilibs:
            # Make sure that a compiler for the host triple can be found
            if args.host != build_platform.triple():
                host_targets.append(build_platform.triple())
        else:
            for t in HOST_TARGETS_DEFAULT:
                if t != args.host and not ("musl" in args.host and "gnu" in t):
                    host_targets.append(t)

    bare_targets: list[str] = []
    if args.bare_targets:
        bare_targets = BARE_TARGETS_DEFAULT

    device_targets: list[str] = []
    if args.device_targets:
        device_targets = DEVICE_TARGETS_DEFAULT

    all_targets: list[str] = host_targets + bare_targets + device_targets

    for target in all_targets:
        if target not in TARGET_CC_FLAGS:
            TARGET_CC_FLAGS[target] = []
        if target not in TARGET_CXX_FLAGS:
            TARGET_CXX_FLAGS[target] = []
        if target not in TARGET_LD_FLAGS:
            TARGET_LD_FLAGS[target] = []

    #
    # Flag set definitions
    #

    common_cc_flags = []
    # `-fuse-ld=lld` will cause Clang to use the prebuilt version of LLD.  This
    # is not desired on Darwin hosts as only the system linker can successfully
    # link binaries for the platform.  As such this flag is not included here
    # and is instead listed in `bare_ld_flags`, `device_ld_flags` and specific
    # host target flags.
    common_ld_flags = []

    host_common_flags = []
    host_cc_flags = [CC_FLAG_PIC]
    # All flags from host_cc_flags will be added to host_cxx_flags
    host_cxx_flags = [
        "--stdlib=libc++",
        f"-I{args.clang_prebuilt.cxxstd()}",
    ]
    host_ld_flags = [
        LD_FLAG_PIC,
    ]

    bare_cc_flags: list[str] = []
    bare_ld_flags: list[str] = [LD_FLAG_USE_LLD]

    device_common_flags = [
        f"--sysroot={(args.ndk_path / NDK_LLVM_PATH_SUFFIX / NDK_SYSROOT_PATH_SUFFIX)}"
    ]
    device_cc_flags = [CC_FLAG_PIC]
    # No need to pass `--rtlib=compiler-rt -lunwind` arguments here because NDK
    # r23+ only has compiler-rt
    device_ld_flags = [
        LD_FLAG_USE_LLD,
        LD_FLAG_PIC,
    ]

    llvm_common_flags = []
    llvm_cc_flags: list[str] = []
    llvm_ld_flags = []

    env_cflags: list[str] = []
    env_rustflags: list[str] = [
        "-Crelocation-model=pic",
        "-Csave-temps",
        f"-Cdlltool={args.clang_prebuilt.dlltool()}",
    ] + ([] if args.verbose else ["-Awarnings"])
    env_rustflags_bootstrap: list[str] = []
    env_rustflags_not_bootstrap: list[str] = []
    env_rustdocflags: list[str] = [
        # For some reason, rustdoc needs a linker.
        f"-Clinker={get_wrapper_paths(args.host)[2]}"
    ]

    #
    # Build platform based configuration
    #

    if build_platform.is_darwin():
        host_common_flags.append(MACOSX_VERSION_FLAG)

    # The Linux build of clang has some architecture-specific libraries, and it knows how
    # to find them. The Windows build doesn't have these, and so it doesn't know how to find
    # libunwind.a. So, when building on Windows, we need to point to the right directory in
    # in the Linux build of clang.
    if build_platform.is_windows():
        device_lib = CLANG_TOOLCHAIN_LINUX.device_lib()
        assert (device_lib is not None)

        for target, arch in DEVICE_TARGET_AND_ARCH.items():
            unwind_path = device_lib / arch
            if not unwind_path.is_dir():
                raise RuntimeError(f"{unwind_path} is not a directory")

            flag = f"-L{unwind_path}"
            if target in TARGET_LD_FLAGS:
                TARGET_LD_FLAGS[target].append(flag)
            else:
                TARGET_LD_FLAGS[target] = [flag]

    TARGET_LD_FLAGS["i686-unknown-linux-gnu"].append(
        f"-L{args.clang_prebuilt.lib() / 'i386-unknown-linux-gnu'}")
    TARGET_LD_FLAGS["i686-unknown-linux-musl"].append(
        f"-L{args.clang_prebuilt.lib() / 'i686-unknown-linux-musl'}")
    TARGET_LD_FLAGS["x86_64-unknown-linux-gnu"].append(
        f"-L{args.clang_prebuilt.lib() / 'x86_64-unknown-linux-gnu'}")
    TARGET_LD_FLAGS["x86_64-unknown-linux-musl"].append(
        f"-L{args.clang_prebuilt.lib() / 'x86_64-unknown-linux-musl'}")

    for target in all_targets:
        if "android" not in target:
            if target.endswith("musl"):
                TARGET_LD_FLAGS[target].append(f"-L{args.clang_prebuilt.musl_lib()}")
            else:
                TARGET_LD_FLAGS[target].append(f"-L{args.clang_prebuilt.lib()}")

            if args.llvm_prebuilt and args.llvm_prebuilt.base_dir != args.clang_prebuilt.base_dir:
                TARGET_LD_FLAGS[target].append(f"-L{args.llvm_prebuilt.lib()}")
                TARGET_LD_FLAGS[target].append(f"-L{args.llvm_prebuilt.runtimes_path(target)}")

    #
    # Command-line based configuration
    #

    # LTO

    llvm_lto_config = "false"
    rust_lto_config = "thin-local"
    if args.lto == "thin":
        llvm_lto_config = "true"
        rust_lto_config = "thin"

    # PGO

    llvm_pgo_config: str = ""
    rustc_pgo_config: str = ""
    if args.profile_generate != None:
        profile_name_llvm = PROFILE_SUBDIR_LLVM if args.llvm_linkage == "shared" else PROFILE_SUBDIR_RUST

        llvm_cc_flags.append(vp_counters_flags(16))
        llvm_cc_flags.append("-Wno-unused-command-line-argument")

        env_rustflags.append("-Cllvm-args=-vp-counters-per-site=16")

        llvm_pgo_config = f"profile-generate = \"{args.profile_generate / profile_name_llvm}\""
        rustc_pgo_config = f"profile-generate = \"{args.profile_generate / PROFILE_SUBDIR_RUST}\""

        if args.llvm_prebuilt:
            env_new["LLVM_PROFILE_FILE"] = (
                args.profile_generate / profile_name_llvm / "%p.profraw").as_posix()

    elif args.profile_use != None:

        # LLVM
        profile_path_llvm: Path = args.profile_use / PROFILE_NAME_LLVM_CS
        if not profile_path_llvm.exists():
            profile_path_llvm = args.profile_use / PROFILE_NAME_LLVM

        if profile_path_llvm.exists():
            llvm_pgo_config = f"profile-use = \"{profile_path_llvm}\""

        # Rust
        profile_path_rust: Path = args.profile_use / PROFILE_NAME_RUST
        if profile_path_rust.exists():
            rustc_pgo_config = f"profile-use = \"{profile_path_rust}\""

        if not (profile_path_llvm.exists() or profile_path_rust.exists()):
            raise ScriptException(f"No profiles found in specified directory: {args.profile_use}")

    if args.cs_profile_generate != None:
        # TODO: The vp-counters-per-site value needs to be tuned to eliminate
        #       warnings about the inability to allocate counters during
        #       context-sensitive profiling.

        if args.llvm_linkage == "shared":
            profile_path = args.cs_profile_generate / PROFILE_SUBDIR_LLVM_CS
            llvm_common_flags += [
                f"-fcs-profile-generate={profile_path}",
                vp_counters_flags(32),
            ]

        else: # args.llvm_linkage == "static"
            if args.lto == None:
                raise RuntimeError(
                    "Context-sensitive PGO with static LLVM linkage requires LTO to be enabled")

            profile_path = args.cs_profile_generate / PROFILE_SUBDIR_RUST

            llvm_common_flags += [
                f"-fcs-profile-generate={profile_path}",
                vp_counters_flags(32),
            ]
            llvm_ld_flags += [
                " -Wl,--lto-cs-profile-generate",
                f"-Wl,--lto-cs-profile-file={profile_path}",
            ]

            env_rustflags += [
                f"-C link-arg=-fcs-profile-generate={profile_path}",
                " -C link-arg=-Wl,--lto-cs-profile-generate",
                f"-C link-arg=-Wl,--lto-cs-profile-file={profile_path}"
            ]

    # Misc.

    if args.emit_relocs:
        for target in HOST_TARGETS_DEFAULT:
            # "-Wl,--emit-relocs" is not supported on Windows targets
            if "linux" in target:
                TARGET_LD_FLAGS[target].append("-Wl,--emit-relocs")
                TARGET_RUSTFLAGS[target].append("-Cstrip=debuginfo")

    if build_platform.is_linux():
        common_cc_flags += ["-ffunction-sections", "-fdata-sections"]
        common_ld_flags.append("-Wl,--gc-sections")

    if args.cgu1:
        env_rustflags.append("-Ccodegen-units=1")

    llvm_link_shared = "true" if args.llvm_linkage == "shared" else "false"

    if args.zstd_library is not None:
        zstd_config = f"zstd_LIBRARY = \"{args.zstd_library}\"\n"
        zstd_config += f"zstd_INCLUDE_DIR = \"{args.zstd_include}\""
    else:
        zstd_config = ""

    # Disable ZSTD if we are using the in-tree LLVM and building for Windows
    # or a MUSL Linux host.
    if (args.unified_prebuilt is None) and (build_platform.is_windows() or
                                            ("musl" in args.host and args.llvm_linkage == "static")):
        zstd_enable = "false"
    else:
        zstd_enable = "true"

    # Coalesce flags

    host_cc_flags = common_cc_flags + host_common_flags + host_cc_flags
    host_cxx_flags = host_cc_flags + host_cxx_flags
    host_ld_flags = common_ld_flags + host_common_flags + host_ld_flags

    bare_cc_flags = common_cc_flags + bare_cc_flags
    bare_ld_flags = common_ld_flags + bare_ld_flags

    device_cc_flags = common_cc_flags + device_common_flags + device_cc_flags
    device_ld_flags = common_ld_flags + device_common_flags + device_ld_flags

    llvm_cc_flags_str = " ".join(llvm_common_flags + llvm_cc_flags)
    llvm_ld_flags_str = " ".join(host_ld_flags + llvm_common_flags + llvm_ld_flags)

    env_new["CFLAGS"] = " ".join(env_cflags)
    env_new["RUSTFLAGS_BOOTSTRAP"] = " ".join(env_rustflags + env_rustflags_bootstrap)
    env_new["RUSTFLAGS_NOT_BOOTSTRAP"] = " ".join(env_rustflags + env_rustflags_not_bootstrap)
    env_new["RUSTDOCFLAGS"] = " ".join(env_rustdocflags)

    # Display final flag strings (and set some straggling env_new vars)

    print()
    print("Compiler/Linker Flags")
    print()
    print(f"env PATH: {env_new['PATH']}")
    print(f"env LIBRARY_PATH: {env_new.get('LIBRARY_PATH', '')}")
    print()
    print(f"env CFLAGS                 : {env_new['CFLAGS']}")
    print(f"env RUSTFLAGS_BOOTSTRAP    : {env_new['RUSTFLAGS_BOOTSTRAP']}")
    print(f"env RUSTFLAGS_NOT_BOOTSTRAP: {env_new['RUSTFLAGS_NOT_BOOTSTRAP']}")
    print()
    print(f"LLVM CC Flags: {llvm_cc_flags_str}")
    print(f"LLVM LD Flags: {llvm_ld_flags_str}")
    print()

    # The LLVM build needs different linker flags for x86_64-unknown-linux-gnu and
    # x86_64-unknown-linux-musl when cross-compiling host musl tools.  The LLVM
    # build never uses the linker wrapper, it runs the compiler wrappers to link
    # instead, so the target-specific linker flags can't go in the wrappers.  It
    # only has a single field in config.toml for all architectures.  The only way
    # to pass target-specific linker flags to LLVM is to use LDFLAGS_<triple>
    # environment variables.
    for target in host_targets:
        env_var_name = "LDFLAGS_" + target.replace("-", "_")
        env_new[env_var_name] = " ".join(TARGET_COMMON_FLAGS[target] + TARGET_LD_FLAGS[target])
        print(f"env {env_var_name} = {env_new[env_var_name]}")
    print()

    for target, flags in TARGET_RUSTFLAGS.items():
        if target in all_targets:
            key = f"CARGO_TARGET_{target.replace('-', '_').upper()}_RUSTFLAGS"
            env_new[key] = " ".join(flags)
            print(f"env {key}: {env_new[key]}")
    print()

    #
    # Instantiate wrappers
    #

    host_configs = "\n".join([
        host_config(target, host_cc_flags, host_cxx_flags, host_ld_flags, env_new, args) for target in host_targets
    ])
    bare_configs = "\n".join([
        bare_config(target, bare_cc_flags, bare_ld_flags, env_new, args) for target in bare_targets
    ])
    device_configs = "\n".join([
        device_config(target, device_cc_flags, device_ld_flags, env_new, args) for target in device_targets
    ])

    quoted_target_list = ",".join([f'\"{target}\"' for target in all_targets])

    #
    # Print Environment
    #

    print()
    print("Inherited Environment Variables")
    for key, val in sorted(env_init.items()):
        print(f"\t{key} => {val}")
    print()

    print()
    print("New Environment Variables")
    with open(args.dist_path / BUILD_ENV_RECORD_NAME, "w") as env_file:
        for key, val in sorted(env_new.items()):
            print(f"\t{key} => {val}")
            env_file.write(f"export {key}=\"{val}\"\n")

    print()

    env_init.update(env_new)

    # as_posix() is necessary here to prevent the backslashes from being
    # interpreted as the start of an esacape sequence on Windows hosts.
    instantiate_template_file(
        CONFIG_TOML_TEMPLATE,
        OUT_PATH_RUST_SOURCE / "config.toml",
        llvm_link_shared=llvm_link_shared,
        llvm_cflags=llvm_cc_flags_str,
        llvm_cxxflags=llvm_cc_flags_str,
        llvm_ldflags=llvm_ld_flags_str,
        llvm_lto_config=llvm_lto_config,
        llvm_cxx_runtime_path_host=args.clang_prebuilt.lib().as_posix(),
        llvm_pgo_config=llvm_pgo_config,
        zstd_config=zstd_config,
        zstd_enable=zstd_enable,
        stage0_host_triple=args.rust_stage0_triple,
        new_host_triple=args.host,
        all_targets=quoted_target_list,
        cargo=args.rust_prebuilt.cargo().as_posix(),
        rustc=args.rust_prebuilt.rustc().as_posix(),
        python=BIN_PATH_PYTHON.as_posix(),
        verbose=1 if args.verbose else 0,
        description=f"\"Android Rust Toolchain version {args.build_name}\"",
        rust_lto_config=rust_lto_config,
        rust_pgo_config=rustc_pgo_config,
        host_configs=host_configs,
        bare_configs=bare_configs,
        device_configs=device_configs)
