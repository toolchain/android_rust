# Copyright (C) 2019 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Provides path expansion to components needed for the rustc build."""

import os
from pathlib import Path
import shutil

import android_rust.build_platform as build_platform

RUST_STAGE0_VERSION: str = "1.83.0"
RUST_STAGE0_TRIPLE_DEFAULT: str
match build_platform.system():
    case build_platform.PlatformType.DARWIN:
        RUST_STAGE0_TRIPLE_DEFAULT = "x86_64-apple-darwin"
    case build_platform.PlatformType.LINUX:
        RUST_STAGE0_TRIPLE_DEFAULT = "x86_64-unknown-linux-gnu"
    case build_platform.PlatformType.WINDOWS:
        RUST_STAGE0_TRIPLE_DEFAULT = "x86_64-pc-windows-gnu"

CLANG_REVISION:   str = "r536225"
CLANG_NAME:       str = f"clang-{CLANG_REVISION}"
GLIBC_VERSION:    str = "2.17-4.8"
GLIBC_SUBVERSION: str = "4.8.3"
MINGW_VERSION:    str = "4.8"

TOOLCHAIN_PATH:           Path = Path(__file__).parents[2].resolve()
TOOLCHAIN_ARTIFACTS_PATH: Path = TOOLCHAIN_PATH / "artifacts"
TOOLCHAIN_RESOURCE_PATH:  Path = TOOLCHAIN_PATH / "resources"
TOOLCHAIN_BAZEL_PATH:     Path = TOOLCHAIN_RESOURCE_PATH / "BUILD.bazel"
WORKSPACE_PATH:           Path = (TOOLCHAIN_PATH / ".." / "..").resolve()
RUST_SOURCE_PATH:         Path = (TOOLCHAIN_PATH / ".." / "rustc").resolve()

CARGO_DENY_CONFIG_PATH: Path = TOOLCHAIN_RESOURCE_PATH / "cargo_deny.toml"

# We take DIST_DIR through an environment variable rather than an
# argument to match the interface for traditional Android builds.
DIST_PATH_DEFAULT: Path = (
    Path(os.environ["DIST_DIR"]).resolve() if "DIST_DIR" in os.environ else
    (WORKSPACE_PATH / "dist"))

BUILD_COMMAND_RECORD_NAME = "rust_build_command.sh"
BUILD_ENV_RECORD_NAME = "rust_build_environment.sh"

PATCHES_PATH:   Path = TOOLCHAIN_PATH / "patches"
TEMPLATES_PATH: Path = TOOLCHAIN_PATH / "templates"

OUT_PATH:                       Path = WORKSPACE_PATH / "out"
OUT_PATH_BIN_LINKS:             Path = OUT_PATH / "rust-toolchain" / "bin"
OUT_PATH_RUST_SOURCE:           Path = OUT_PATH / "rustc"
OUT_PATH_CARGO_HOME:            Path = OUT_PATH / "cargo_home"
OUT_PATH_RUST_BOOTSTRAP_SOURCE: Path = OUT_PATH_RUST_SOURCE / "src" / "bootstrap"
OUT_PATH_RUST_CARGO_SOURCE:     Path = OUT_PATH_RUST_SOURCE / "src" / "tools" / "cargo"
OUT_PATH_PACKAGE:               Path = OUT_PATH / "package"
OUT_PATH_PATCHS_LOG:            Path = OUT_PATH / "patches.log"
OUT_PATH_PATCHED_FILES:         Path = OUT_PATH / "patched-files"
OUT_PATH_PROFILES:              Path = OUT_PATH / "profiles"
OUT_PATH_STDLIB_SRCS_RELATIVE:  Path = Path("lib") / "rustlib" / "src" / "rust"
OUT_PATH_STDLIB_SRCS:           Path = OUT_PATH_PACKAGE / OUT_PATH_STDLIB_SRCS_RELATIVE
OUT_PATH_TRACE:                 Path = OUT_PATH / "build.trace.gz"
OUT_PATH_WRAPPERS:              Path = OUT_PATH / "wrappers"
OUT_PATH_LLVM_LIB_DIR:          Path = OUT_PATH_RUST_SOURCE / "build" / build_platform.triple() / "llvm" / "lib"

PROFILE_SUBDIR_BOLT    = Path("bolt")
PROFILE_SUBDIR_LLVM    = Path("llvm")
PROFILE_SUBDIR_LLVM_CS = Path("llvm-cs")
PROFILE_SUBDIR_RUST    = Path("rust")
PROFILE_NAME_LLVM      = "llvm.profdata"
PROFILE_NAME_LLVM_CS   = "llvm-cs.profdata"
PROFILE_NAME_RUST      = "rust.profdata"

PROFILE_SUBDIRS = [
    PROFILE_SUBDIR_LLVM,
    PROFILE_SUBDIR_LLVM_CS,
    PROFILE_SUBDIR_RUST
]

PROFILE_NAMES = [
    PROFILE_NAME_LLVM,
    PROFILE_NAME_LLVM_CS,
    PROFILE_NAME_RUST
]

PROFILED_VERSION_FILENAME = "profiled_version.txt"

BOLT_LOG_NAME: str  = "bolt.rust.log"

DOWNLOADS_PATH: Path = WORKSPACE_PATH / ".downloads"

SOONG_UI_PATH: Path = WORKSPACE_PATH / "build" / "soong" / "soong_ui.bash"

PREBUILT_PATH:                     Path = WORKSPACE_PATH / "prebuilts"
RUST_PREBUILT_PATH:                Path = PREBUILT_PATH / "rust"
RUST_HOST_STAGE0_PATH:             Path = RUST_PREBUILT_PATH / build_platform.prebuilt() / RUST_STAGE0_VERSION
LLVM_PREBUILT_PATH_HOST:           Path = PREBUILT_PATH / "clang" / "host" / build_platform.prebuilt() / CLANG_NAME
LLVM_PREBUILT_PATH_LINUX:          Path = PREBUILT_PATH / "clang" / "host" / "linux-x86" / CLANG_NAME
LLVM_PREBUILT_PATH_WINDOWS:        Path = PREBUILT_PATH / "clang" / "host" / "windows-x86" / CLANG_NAME

GCC_TOOLCHAIN_PATH:  Path = PREBUILT_PATH / "gcc" / build_platform.prebuilt() / "host" / ("x86_64-linux-glibc" + GLIBC_VERSION)
GCC_LIB_PATH:        Path = GCC_TOOLCHAIN_PATH / "x86_64-linux" / "lib64"
GCC_LIBGCC_PATH:     Path = GCC_TOOLCHAIN_PATH / "lib" / "gcc" / "x86_64-linux" / GLIBC_SUBVERSION
GCC_SYSROOT_PATH:    Path = GCC_TOOLCHAIN_PATH / "sysroot"
GCC_PKG_CONFIG_PATH: Path = GCC_SYSROOT_PATH / "usr" / "lib" / "pkgconfig"

MINGW_TOOLCHAIN_PATH: Path = PREBUILT_PATH / "gcc" / "linux-x86" / "host" / ("x86_64-w64-mingw32-" + MINGW_VERSION)
MINGW_LIB_PATH:       Path = MINGW_TOOLCHAIN_PATH / "x86_64-w64-mingw32" / "lib64"
MINGW_LIBGCC_PATH:    Path = MINGW_TOOLCHAIN_PATH / "lib" / "gcc" / "x86_64-w64-mingw32" / GLIBC_SUBVERSION
MINGW_SYSROOT_PATH:   Path = MINGW_TOOLCHAIN_PATH / "x86_64-w64-mingw32"

MUSL_SYSROOT64_PATH: Path = PREBUILT_PATH / "build-tools" / "sysroots" / "x86_64-unknown-linux-musl"
MUSL_SYSROOT32_PATH: Path = PREBUILT_PATH / "build-tools" / "sysroots" / "i686-unknown-linux-musl"

BUILD_TOOLS_PREBUILT_MULTICALL: Path = PREBUILT_PATH / "build-tools" / "path" / build_platform.prebuilt()
BUILD_TOOLS_PREBUILT_BIN_PATH:  Path = PREBUILT_PATH / "build-tools" / build_platform.prebuilt() / "bin"
CMAKE_PREBUILT_PATH:            Path = PREBUILT_PATH / "cmake" / build_platform.prebuilt()
NINJA_PREBUILT_PATH:            Path = PREBUILT_PATH / "ninja" / build_platform.prebuilt()
PYTHON_PREBUILT_PATH:           Path = PREBUILT_PATH / "python" / build_platform.prebuilt()

# Use of the NDK should eventually be removed so as to make this a Platform
# target, but is used for now as a transition stage.
NDK_VERSION         = "r28"
NDK_DIRECTORY_NAME  = f"android-ndk-{NDK_VERSION}"
NDK_RELEASE_ARCHIVE = f"{NDK_DIRECTORY_NAME}-linux.zip"
NDK_DOWNLOAD_URL    = f"https://dl.google.com/android/repository/{NDK_RELEASE_ARCHIVE}"

NDK_LLVM_PATH_SUFFIX:    Path = Path("toolchains") / "llvm" / "prebuilt" / "linux-x86_64"
NDK_SYSROOT_PATH_SUFFIX: Path = Path("sysroot")

SOONG_PATH: Path = WORKSPACE_PATH / "build" / "soong"

#
# Paths to utilities used by the build script.
#

BIN_PATH_TOYBOX:  Path = BUILD_TOOLS_PREBUILT_BIN_PATH / "toybox"
BIN_PATH_ZIPTOOL: Path = BUILD_TOOLS_PREBUILT_BIN_PATH / "ziptool"
BIN_PATH_XZ:      Path = BUILD_TOOLS_PREBUILT_BIN_PATH / "xz"

LINK_PATH_CC:    Path = OUT_PATH_BIN_LINKS / "cc"
LINK_PATH_PERL:  Path = OUT_PATH_BIN_LINKS / "perl"
LINK_PATH_SH:    Path = OUT_PATH_BIN_LINKS / "sh"
LINK_PATH_STRIP: Path = OUT_PATH_BIN_LINKS / "strip"
LINK_PATH_UNZIP: Path = OUT_PATH_BIN_LINKS / "unzip"

if build_platform.is_windows():
    GIT_USR_BIN_PATH: Path = Path("C:\\") / "program files" / "git" / "usr" / "bin"

    BIN_PATH_PYTHON: Path = PYTHON_PREBUILT_PATH  / "python.exe"
    BIN_PATH_FILE:   Path = GIT_USR_BIN_PATH / "file.exe"
    BIN_PATH_PATCH:  Path = GIT_USR_BIN_PATH / "patch.exe"
    BIN_PATH_TAR:    Path = Path("C:\\") / "phony" / "path" / "do" / "not" / "use"
else:
    BIN_PATH_PYTHON: Path = PYTHON_PREBUILT_PATH  / "bin" / "python3"
    BIN_PATH_FILE:   Path = BUILD_TOOLS_PREBUILT_MULTICALL / "file"
    BIN_PATH_PATCH:  Path = shutil.which("patch")
    BIN_PATH_TAR:    Path = BUILD_TOOLS_PREBUILT_MULTICALL / "tar"

#
# Paths to binfs executables
#

ANDROID_BUILD_CLI_PATH: Path = Path("/google/data/ro/projects/android/ab")
FETCH_ARTIFACT_PATH:    Path = Path("/google/data/ro/projects/android/fetch_artifact")
