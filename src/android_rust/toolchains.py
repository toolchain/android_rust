# Copyright (C) 2024 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import itertools
from pathlib import Path
import re
import subprocess
import sys

from android_rust.build_platform import PlatformType
from android_rust.paths import (
    LLVM_PREBUILT_PATH_HOST,
    LLVM_PREBUILT_PATH_LINUX,
    LLVM_PREBUILT_PATH_WINDOWS,
    PROFILE_NAME_LLVM,
    PROFILE_NAME_LLVM_CS,
    PROFILE_NAME_RUST,
    PROFILE_NAMES,
    PROFILE_SUBDIR_BOLT,
    PROFILE_SUBDIRS,
    PROFILED_VERSION_FILENAME,
    RUST_HOST_STAGE0_PATH)
from android_rust.utils import archive_create, run_and_exit_on_failure, run_quiet_and_exit_on_failure


LLVM_VERSION_DEFAULT: str = "19"

# BOLT profile name examples:
#  * libLLVM-16.so.29587.fdata
#  * librustc_driver-4ebf072145fdffb0.so.192847.fdata
#  * rustc.987846.fdata
PATTERN_BOLT_PROFILED = re.compile("^([\w-]+(?:\.[\w-]+)*)\.\d+\.fdata$")


class ClangToolchain:
    PATH_METHODS = [
        "ar",
        "bolt",
        "clang",
        "clangxx",
        "dlltool",
        "merge_fdata",
        "objcopy",
        "profdata",
        "ranlib",
        "readelf",
    ]

    def __init__(
            self,
            base_dir: Path,
            host: PlatformType | None = None,
            llvm_version: str = LLVM_VERSION_DEFAULT):
        if host is None:
            if "linux-x86" in base_dir.parts:
                host = PlatformType.LINUX
            elif "darwin-x86" in base_dir.parts:
                host = PlatformType.DARWIN
            elif "windows-x86" in base_dir.parts:
                host = PlatformType.WINDOWS
            else:
                raise RuntimeError(f"Toolchain host autodetection failed for {base_dir}")

        self.base_dir = base_dir
        self.bin_dir = base_dir / "bin"
        self.host = host
        self.exe = ".exe" if self.host == PlatformType.WINDOWS else ""
        self.llvm_version = llvm_version

        for method_name in self.__class__.PATH_METHODS:
            path = getattr(self, method_name)()
            if path is not None and not path.exists():
                raise RuntimeError(f"Clang toolchain path does not exist: {path}")

    #
    # Binary Paths
    #

    def ar(self) -> Path:
        return self.bin_dir / ("llvm-ar" + self.exe)

    def bolt(self) -> Path | None:
        if self.is_linux():
            return self.bin_dir / ("llvm-bolt" + self.exe)
        else:
            return None

    def clang(self) -> Path:
        return self.bin_dir / ("clang" + self.exe)

    def clangxx(self) -> Path:
        return self.bin_dir / ("clang++" + self.exe)

    def dlltool(self) -> Path:
        return self.bin_dir / ("llvm-dlltool" + self.exe)

    def merge_fdata(self) -> Path | None:
        if self.is_linux():
            return self.bin_dir / ("merge-fdata" + self.exe)
        else:
            return None

    def objcopy(self) -> Path:
        return self.bin_dir / ("llvm-objcopy" + self.exe)

    def profdata(self) -> Path:
        return self.bin_dir / ("llvm-profdata" + self.exe)

    def ranlib(self) -> Path:
        return self.bin_dir / ("llvm-ranlib" + self.exe)

    def readelf(self) -> Path:
        return self.bin_dir / ("llvm-readelf" + self.exe)

    #
    # Include Paths
    #

    def cxxstd(self) -> Path:
        return self.base_dir / "include" / "c++" / "v1"

    def lib(self) -> Path:
        return self.base_dir / "lib"

    def lib64(self) -> Path:
        return self.base_dir / "lib64"

    def musl_lib(self) -> Path | None:
        if self.is_linux():
            return self.base_dir / "musl" / "lib" / "x86_64-unknown-linux-musl"
        else:
            return None

    def device_lib(self) -> Path | None:
        if self.is_linux():
            return self.base_dir / "lib" / "clang" / self.llvm_version / "lib" / "linux"
        else:
            return None

    def runtimes_path(self, triple: str) -> Path | None:
        if self.is_linux():
            return self.base_dir / "lib" / "clang" / self.llvm_version / "lib" / triple
        else:
            return None

    #
    # Tool Wrappers
    #

    def merge_profiles_pgo(self, inputs: list[Path], outpath: Path) -> None:
        run_and_exit_on_failure(
            [
                self.profdata(),
                "merge",
                "-o",
                outpath,
            ] + inputs,
            f"Failed to produce merged PGO profile {outpath}") # yapf: disable


    def merge_profiles_bolt(self, inputs: list[Path], outpath: Path) -> None:
        run_quiet_and_exit_on_failure(
            [
                self.merge_fdata(),
                "-o",
                outpath,
            ] + inputs,
            f"Failed to produce merged BOLT profiles {outpath}") # yapf: disable


    def strip_symbols(self, obj_path: Path, flag: str = "--strip-unneeded") -> None:
        subprocess.run([str(self.objcopy()), "--keep-section='.rustc'", flag, str(obj_path)],
                       check=True)

    #
    # Helper Functions
    #

    def is_linux(self) -> bool:
        return self.host == PlatformType.LINUX

    def is_windows(self) -> bool:
        return self.host == PlatformType.WINDOWS


class RustToolchain:
    PATH_METHODS = [
        "cargo",
        "rustc",
    ]

    def __init__(self, base_dir: Path, host: PlatformType | None = None):
        if host is None:
            if "linux-x86" in base_dir.parts:
                host = PlatformType.LINUX
            elif "darwin-x86" in base_dir.parts:
                host = PlatformType.DARWIN
            elif "windows-x86" in base_dir.parts:
                host = PlatformType.WINDOWS
            else:
                raise RuntimeError(f"Toolchain host autodetection failed for {base_dir}")

        self.base_dir = base_dir
        self.bin_dir = base_dir / "bin"
        self.host = host
        self.exe = ".exe" if self.host == PlatformType.WINDOWS else ""

        for method_name in self.__class__.PATH_METHODS:
            path = getattr(self, method_name)()
            if path is not None and not path.exists():
                raise RuntimeError(f"Rust toolchain path does not exist: {path}")

    #
    # Binary Paths
    #

    def cargo(self) -> Path:
        return self.bin_dir / ("cargo" + self.exe)

    def rustc(self) -> Path:
        return self.bin_dir / ("rustc" + self.exe)

    #
    # Helper Functions
    #

    def is_linux(self) -> bool:
        return self.host == PlatformType.LINUX

    def is_windows(self) -> bool:
        return self.host == PlatformType.WINDOWS

    def get_version(self) -> str:
        return subprocess.run([str(self.rustc()), "--version"],
                              stdout=subprocess.PIPE).stdout.decode("utf-8").strip()


CLANG_TOOLCHAIN_HOST = ClangToolchain(LLVM_PREBUILT_PATH_HOST)
CLANG_TOOLCHAIN_LINUX = ClangToolchain(LLVM_PREBUILT_PATH_LINUX)

CLANG_TOOLCHAIN_WINDOWS: ClangToolchain | None
if LLVM_PREBUILT_PATH_WINDOWS.exists():
    CLANG_TOOLCHAIN_WINDOWS = ClangToolchain(LLVM_PREBUILT_PATH_WINDOWS)
else:
    CLANG_TOOLCHAIN_WINDOWS = None

RUST_TOOLCHAIN_HOST = RustToolchain(RUST_HOST_STAGE0_PATH)

#
# Profile helpers
#


def check_bolt_profile_dirs(indirs: list[Path]) -> None:
    for (dir0, dir1) in itertools.pairwise(indirs):
        dir0_list = [p.name for p in dir0.iterdir()]
        dir1_list = [p.name for p in dir1.iterdir()]
        if dir0_list != dir1_list:
            print("Error: Directory contents mismatch")
            print(str(dir0))
            for p in dir0_list:
                print(f"\t{p}")
            print(str(dir1))
            for p in dir1_list:
                print(f"\t{p}")

            sys.exit(1)


def merge_profile_dirs_bolt(toolchain: ClangToolchain, indirs: list[Path], outdir: Path) -> None:
    check_bolt_profile_dirs(indirs)

    outdir.mkdir(exist_ok=True)
    for path in indirs[0].glob("*.fdata"):
        toolchain.merge_profiles_bolt([dir / path.name for dir in indirs], outdir / path.name)


def merge_profile_dirs_pgo(
        toolchain: ClangToolchain, indirs: list[Path], input_names: list[str],
        outdir: Path) -> None:
    inputs: list[Path] = []
    for indir in indirs:
        for name in input_names:
            inputs += indir.glob(f"**/{name}")

    if inputs:
        toolchain.merge_profiles_pgo(inputs, outdir)


def export_profiles(
        toolchain: ClangToolchain,
        src_path: Path | None,
        dist_path: Path,
        version: str | None = None) -> None:
    """
    Merges the PID-tagged profiles generated during an invocation of either
    build.py or test_compiler.py into a set of canonical profiles.
    """
    if src_path is not None:
        if src_path.name == PROFILE_SUBDIR_BOLT.name:
            bolt_output_path = dist_path / PROFILE_SUBDIR_BOLT
            bolt_output_path.mkdir(exist_ok=True, parents=True)

            if version is not None:
                with open(bolt_output_path / PROFILED_VERSION_FILENAME, "w") as version_file:
                    version_file.write(version)

            instrumented_binaries = set()
            for profile_path in src_path.glob("*.fdata"):
                match = PATTERN_BOLT_PROFILED.search(profile_path.name)
                if match is not None:
                    instrumented_binaries.add(match.group(1))

            for binary in instrumented_binaries:
                toolchain.merge_profiles_bolt(
                    list(src_path.glob(f"{binary}.*.fdata")), bolt_output_path / f"{binary}.fdata")

            archive_create(
                dist_path / "bolt-profiles", dist_path, PROFILE_SUBDIR_BOLT, overwrite=True)

        else:
            for subdir, profile_name in zip(PROFILE_SUBDIRS, PROFILE_NAMES):
                if (src_path / subdir).exists():
                    toolchain.merge_profiles_pgo(
                        list((src_path / subdir).glob("*.profraw")), dist_path / profile_name)


def merge_project_profiles(toolchain: ClangToolchain, indirs: list[Path], outdir: Path) -> None:
    """
    Merge either the BOLT or PGO profiles from different tool invocations
    together into profiles with the same name in the outdir.
    """
    if (indirs[0] / PROFILE_SUBDIR_BOLT).exists():
        print("Merging BOLT profiles")
        merge_profile_dirs_bolt(
            toolchain, [dir / PROFILE_SUBDIR_BOLT for dir in indirs], outdir / PROFILE_SUBDIR_BOLT)
        archive_create(outdir / "bolt-profiles", outdir, PROFILE_SUBDIR_BOLT, overwrite=True)
    else:
        merge_profile_dirs_pgo(
            toolchain,
            indirs, [PROFILE_NAME_LLVM, PROFILE_NAME_LLVM_CS],
            outdir / PROFILE_NAME_LLVM)
        merge_profile_dirs_pgo(toolchain, indirs, [PROFILE_NAME_RUST], outdir / PROFILE_NAME_RUST)
