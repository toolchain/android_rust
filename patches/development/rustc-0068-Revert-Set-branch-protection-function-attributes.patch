From 9dffa7293273a11390feec3867f932557455073e Mon Sep 17 00:00:00 2001
From: Chris Wailes <chriswailes@google.com>
Date: Mon, 2 Dec 2024 15:49:04 -0800
Subject: [PATCH 13/14] Revert "Set branch protection function attributes"

This reverts commit ea7625f4266e69498ea5b9287a72e543a514a837.
---
 compiler/rustc_codegen_llvm/src/attributes.rs | 36 ++-------------
 tests/codegen/branch-protection-old-llvm.rs   | 45 -------------------
 tests/codegen/branch-protection.rs            | 10 -----
 3 files changed, 3 insertions(+), 88 deletions(-)
 delete mode 100644 tests/codegen/branch-protection-old-llvm.rs

diff --git a/compiler/rustc_codegen_llvm/src/attributes.rs b/compiler/rustc_codegen_llvm/src/attributes.rs
index 2c5ec9dad5..bf9d1337e1 100644
--- a/compiler/rustc_codegen_llvm/src/attributes.rs
+++ b/compiler/rustc_codegen_llvm/src/attributes.rs
@@ -1,18 +1,18 @@
 //! Set and unset common attributes on LLVM values.
 
 use rustc_attr::{InlineAttr, InstructionSetAttr, OptimizeAttr};
 use rustc_codegen_ssa::traits::*;
 use rustc_hir::def_id::DefId;
 use rustc_middle::middle::codegen_fn_attrs::{CodegenFnAttrFlags, PatchableFunctionEntry};
 use rustc_middle::ty::{self, TyCtxt};
-use rustc_session::config::{BranchProtection, FunctionReturn, OptLevel, PAuthKey, PacRet};
+use rustc_session::config::{FunctionReturn, OptLevel};
 use rustc_target::spec::{FramePointer, SanitizerSet, StackProbeType, StackProtector};
 use smallvec::SmallVec;
 
 use crate::context::CodegenCx;
 use crate::errors::SanitizerMemtagRequiresMte;
 use crate::llvm::AttributePlace::Function;
 use crate::llvm::{self, AllocKindFlags, Attribute, AttributeKind, AttributePlace, MemoryEffects};
 use crate::value::Value;
 use crate::{attributes, llvm_util};
 
@@ -399,52 +399,22 @@ pub(crate) fn llfn_attrs_from_instance<'ll, 'tcx>(
     if codegen_fn_attrs.flags.contains(CodegenFnAttrFlags::FFI_CONST) {
         to_add.push(MemoryEffects::None.create_attr(cx.llcx));
     }
     if codegen_fn_attrs.flags.contains(CodegenFnAttrFlags::NAKED) {
         to_add.push(AttributeKind::Naked.create_attr(cx.llcx));
         // HACK(jubilee): "indirect branch tracking" works by attaching prologues to functions.
         // And it is a module-level attribute, so the alternative is pulling naked functions into
         // new LLVM modules. Otherwise LLVM's "naked" functions come with endbr prefixes per
         // https://github.com/rust-lang/rust/issues/98768
         to_add.push(AttributeKind::NoCfCheck.create_attr(cx.llcx));
-        if llvm_util::get_version() < (19, 0, 0) {
-            // Prior to LLVM 19, branch-target-enforcement was disabled by setting the attribute to
-            // the string "false". Now it is disabled by absence of the attribute.
-            to_add.push(llvm::CreateAttrStringValue(cx.llcx, "branch-target-enforcement", "false"));
-        }
-    } else {
-        // Do not set sanitizer attributes for naked functions.
-        to_add.extend(sanitize_attrs(cx, codegen_fn_attrs.no_sanitize));
-
-        if llvm_util::get_version() >= (19, 0, 0) {
-            // For non-naked functions, set branch protection attributes on aarch64.
-            if let Some(BranchProtection { bti, pac_ret }) =
-                cx.sess().opts.unstable_opts.branch_protection
-            {
-                assert!(cx.sess().target.arch == "aarch64");
-                if bti {
-                    to_add.push(llvm::CreateAttrString(cx.llcx, "branch-target-enforcement"));
-                }
-                if let Some(PacRet { leaf, key }) = pac_ret {
-                    to_add.push(llvm::CreateAttrStringValue(
-                        cx.llcx,
-                        "sign-return-address",
-                        if leaf { "all" } else { "non-leaf" },
-                    ));
-                    to_add.push(llvm::CreateAttrStringValue(
-                        cx.llcx,
-                        "sign-return-address-key",
-                        if key == PAuthKey::A { "a_key" } else { "b_key" },
-                    ));
-                }
-            }
-        }
+        // Need this for AArch64.
+        to_add.push(llvm::CreateAttrStringValue(cx.llcx, "branch-target-enforcement", "false"));
     }
     if codegen_fn_attrs.flags.contains(CodegenFnAttrFlags::ALLOCATOR)
         || codegen_fn_attrs.flags.contains(CodegenFnAttrFlags::ALLOCATOR_ZEROED)
     {
         to_add.push(create_alloc_family_attr(cx.llcx));
         // apply to argument place instead of function
         let alloc_align = AttributeKind::AllocAlign.create_attr(cx.llcx);
         attributes::apply_to_llfn(llfn, AttributePlace::Argument(1), &[alloc_align]);
         to_add.push(llvm::CreateAllocSizeAttr(cx.llcx, 0));
         let mut flags = AllocKindFlags::Alloc | AllocKindFlags::Aligned;
diff --git a/tests/codegen/branch-protection-old-llvm.rs b/tests/codegen/branch-protection-old-llvm.rs
deleted file mode 100644
index bb3c7a4b70..0000000000
--- a/tests/codegen/branch-protection-old-llvm.rs
+++ /dev/null
@@ -1,45 +0,0 @@
-// Test that the correct module flags are emitted with different branch protection flags.
-
-//@ revisions: BTI PACRET LEAF BKEY NONE
-//@ needs-llvm-components: aarch64
-//@ [BTI] compile-flags: -Z branch-protection=bti
-//@ [PACRET] compile-flags: -Z branch-protection=pac-ret
-//@ [LEAF] compile-flags: -Z branch-protection=pac-ret,leaf
-//@ [BKEY] compile-flags: -Z branch-protection=pac-ret,b-key
-//@ compile-flags: --target aarch64-unknown-linux-gnu
-//@ ignore-llvm-version: 19 - 99
-
-#![crate_type = "lib"]
-#![feature(no_core, lang_items)]
-#![no_core]
-
-#[lang = "sized"]
-trait Sized {}
-
-// A basic test function.
-pub fn test() {}
-
-// BTI: !"branch-target-enforcement", i32 1
-// BTI: !"sign-return-address", i32 0
-// BTI: !"sign-return-address-all", i32 0
-// BTI: !"sign-return-address-with-bkey", i32 0
-
-// PACRET: !"branch-target-enforcement", i32 0
-// PACRET: !"sign-return-address", i32 1
-// PACRET: !"sign-return-address-all", i32 0
-// PACRET: !"sign-return-address-with-bkey", i32 0
-
-// LEAF: !"branch-target-enforcement", i32 0
-// LEAF: !"sign-return-address", i32 1
-// LEAF: !"sign-return-address-all", i32 1
-// LEAF: !"sign-return-address-with-bkey", i32 0
-
-// BKEY: !"branch-target-enforcement", i32 0
-// BKEY: !"sign-return-address", i32 1
-// BKEY: !"sign-return-address-all", i32 0
-// BKEY: !"sign-return-address-with-bkey", i32 1
-
-// NONE-NOT: branch-target-enforcement
-// NONE-NOT: sign-return-address
-// NONE-NOT: sign-return-address-all
-// NONE-NOT: sign-return-address-with-bkey
diff --git a/tests/codegen/branch-protection.rs b/tests/codegen/branch-protection.rs
index 2f5ff9e98c..a29ec67d57 100644
--- a/tests/codegen/branch-protection.rs
+++ b/tests/codegen/branch-protection.rs
@@ -1,54 +1,44 @@
 // Test that the correct module flags are emitted with different branch protection flags.
 
 //@ revisions: BTI PACRET LEAF BKEY NONE
 //@ needs-llvm-components: aarch64
 //@ [BTI] compile-flags: -Z branch-protection=bti
 //@ [PACRET] compile-flags: -Z branch-protection=pac-ret
 //@ [LEAF] compile-flags: -Z branch-protection=pac-ret,leaf
 //@ [BKEY] compile-flags: -Z branch-protection=pac-ret,b-key
 //@ compile-flags: --target aarch64-unknown-linux-gnu
-//@ min-llvm-version: 19
 
 #![crate_type = "lib"]
 #![feature(no_core, lang_items)]
 #![no_core]
 
 #[lang = "sized"]
 trait Sized {}
 
 // A basic test function.
-// CHECK: @test(){{.*}} [[ATTR:#[0-9]+]] {
-#[no_mangle]
 pub fn test() {}
 
-// BTI: attributes [[ATTR]] = {{.*}} "branch-target-enforcement"
 // BTI: !"branch-target-enforcement", i32 1
 // BTI: !"sign-return-address", i32 0
 // BTI: !"sign-return-address-all", i32 0
 // BTI: !"sign-return-address-with-bkey", i32 0
 
-// PACRET: attributes [[ATTR]] = {{.*}} "sign-return-address"="non-leaf"
-// PACRET-SAME: "sign-return-address-key"="a_key"
 // PACRET: !"branch-target-enforcement", i32 0
 // PACRET: !"sign-return-address", i32 1
 // PACRET: !"sign-return-address-all", i32 0
 // PACRET: !"sign-return-address-with-bkey", i32 0
 
-// LEAF: attributes [[ATTR]] = {{.*}} "sign-return-address"="all"
-// LEAF-SAME: "sign-return-address-key"="a_key"
 // LEAF: !"branch-target-enforcement", i32 0
 // LEAF: !"sign-return-address", i32 1
 // LEAF: !"sign-return-address-all", i32 1
 // LEAF: !"sign-return-address-with-bkey", i32 0
 
-// BKEY: attributes [[ATTR]] = {{.*}} "sign-return-address"="non-leaf"
-// BKEY-SAME: "sign-return-address-key"="b_key"
 // BKEY: !"branch-target-enforcement", i32 0
 // BKEY: !"sign-return-address", i32 1
 // BKEY: !"sign-return-address-all", i32 0
 // BKEY: !"sign-return-address-with-bkey", i32 1
 
 // NONE-NOT: branch-target-enforcement
 // NONE-NOT: sign-return-address
 // NONE-NOT: sign-return-address-all
 // NONE-NOT: sign-return-address-with-bkey
-- 
2.48.1.262.g85cc9f2d1e-goog

