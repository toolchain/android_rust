#!/usr/bin/env python3
#
# Copyright (C) 2022 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Merge profiles from multiple Rust toolchain build targets"""

import argparse
from pathlib import Path
from tempfile import TemporaryDirectory

import context

from android_rust.paths import DIST_PATH_DEFAULT
from android_rust.toolchains import CLANG_TOOLCHAIN_HOST, merge_project_profiles
from android_rust.utils import ExtantPath, ResolvedPath, archive_extract, is_archive

from test_compiler import initialize_clang_prebuilt

#
# Program logic
#


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser("Merge profiles from multiple Rust toolchain build targets")
    parser.add_argument(
        "inputs",
        type=ResolvedPath,
        nargs="+",
        help="List of directories or archives in which to search for profiles")
    parser.add_argument(
        "--dist",
        "-d",
        dest="dist_path",
        type=ResolvedPath,
        default=DIST_PATH_DEFAULT,
        help="Where to place distributable artifacts")
    parser.add_argument(
        "--clang-prebuilt",
        type=ExtantPath,
        default=CLANG_TOOLCHAIN_HOST,
        help="Clang toolchain that will be used to merge profile data; must match the LLVM version used to instrument the Rust toolchain")

    return parser.parse_args()


def main() -> None:
    args = parse_args()

    initialize_clang_prebuilt(args)

    expanded_inputs = []
    with TemporaryDirectory() as temp_dir:
        temp_dir_path = Path(temp_dir)
        for idx, input_path in enumerate(args.inputs):
            if input_path.is_dir():
                expanded_inputs.append(input_path)

            elif is_archive(input_path):
                extract_path = temp_dir_path / str(idx)
                extract_path.mkdir()
                print(f"Unpacking {input_path}")
                archive_extract(input_path, extract_path)
                expanded_inputs.append(extract_path)

            else:
                raise RuntimeError(f"Unknown input file type: {input_path}")

        args.dist_path.mkdir(exist_ok=True)
        merge_project_profiles(args.clang_prebuilt, expanded_inputs, args.dist_path)


if __name__ == "__main__":
    main()
