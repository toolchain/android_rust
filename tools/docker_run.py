#!/usr/bin/env python3
#
# Copyright (C) 2024 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Run the Rust toolchain tools inside a Docker image"""

import os
from pathlib import Path
import subprocess
import sys

import context

from android_rust.paths import BIN_PATH_PYTHON, WORKSPACE_PATH
from android_rust.utils import prepare_command


def main() -> None:
    workspace_stats = os.stat(WORKSPACE_PATH)

    if len(sys.argv) <= 1:
        print("Usage: docker-run.py <script path>")
        sys.exit(0)

    docker_invoke = [
        "docker",
        "run",
        "-t",
        "--user",
        f"{workspace_stats.st_uid}:{workspace_stats.st_gid}",
        "--network",
        "none",
        "--mount",
        f"type=bind,source={WORKSPACE_PATH},target=/android",
        "android-rust:dev",
        f"/android/{BIN_PATH_PYTHON.relative_to(WORKSPACE_PATH)}",
    ]

    script_path = Path(sys.argv[1])
    script_invoke = [str(script_path.resolve().relative_to(WORKSPACE_PATH))] + sys.argv[2:]

    if script_path.name == "build.py" and "--ndk" not in script_invoke:
        script_invoke.extend([
            "--ndk",
            "/ndk",
            "--no-cargo-audit",
        ])

    print(" ".join(script_invoke))
    subprocess.run(prepare_command(docker_invoke + script_invoke))


if __name__ == "__main__":
    main()
