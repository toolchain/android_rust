#!/bin/sh
#
# Copyright (C) 2024 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

tools_dir=`realpath $(dirname $0k)`
dispatch_path=`realpath $tools_dir/dispatch.py`
python_path=`realpath $tools_dir/../../../prebuilts/python/linux-x86/bin/python3`

$python_path $dispatch_path $@
