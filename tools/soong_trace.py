#!/usr/bin/env python3
#
# Copyright (C) 2022 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import bisect
import csv
import _csv
from dataclasses import dataclass
import gzip
import json
from pathlib import Path
from typing import Optional, TextIO, Union

import context

from android_rust.utils import ExtantPath, TIME_US_IN_MINUTE, TIME_US_IN_SECOND, dig


TRACE_PATH_PREFIX = "out/soong/.intermediates"

#
# Classes
#


@dataclass
class TraceInfo:
    duration_cc_µs: int = 0
    duration_java_µs: int = 0
    duration_other_µs: int = 0
    duration_rust_µs: int = 0
    duration_total_µs: int = 0
    targets_cc: int = 0
    targets_java: int = 0
    targets_other: int = 0
    targets_rust: int = 0
    targets_total: int = 0

    @staticmethod
    def header() -> list[str]:
        return [
            "Total targets",
            "Total time (m)",
            "CC targets",
            "CC time (m)",
            "CC time percent",
            "CC time per target (s)",
            "Java targets",
            "Java time (m)",
            "Java time percent",
            "Java time per target (s)",
            "Rust targets",
            "Rust time (m)",
            "Rust time percent",
            "Rust time per target (s)",
            "Other targets",
            "Other time (m)",
            "Other time percent",
            "Other time per target (s)",
        ]

    def gather(self) -> list[Union[int, float]]:
        return [
            self.targets_total,
            round(self.duration_total_μs / TIME_US_IN_MINUTE, 2),
            self.targets_cc,
            round(self.duration_cc_μs / TIME_US_IN_MINUTE, 2),
            round((self.duration_cc_µs / self.duration_total_µs) * 100, 2),
            round((self.duration_cc_µs / self.targets_cc) / TIME_US_IN_SECOND, 2),
            self.targets_java,
            round(self.duration_java_μs / TIME_US_IN_MINUTE, 2),
            round((self.duration_java_µs / self.duration_total_µs) * 100, 2),
            round((self.duration_java_µs / self.targets_java) / TIME_US_IN_SECOND, 2),
            self.targets_rust,
            round(self.duration_rust_μs / TIME_US_IN_MINUTE, 2),
            round((self.duration_rust_µs / self.duration_total_µs) * 100, 2),
            round((self.duration_rust_µs / self.targets_rust) / TIME_US_IN_SECOND, 2),
            self.targets_other,
            round(self.duration_other_μs / TIME_US_IN_MINUTE, 2),
            round((self.duration_other_µs / self.duration_total_µs) * 100, 2),
            round((self.duration_other_µs / self.targets_other) / TIME_US_IN_SECOND, 2),
        ]


#
# Helper functions
#


def init_csv(args: argparse.Namespace) -> Optional["_csv._writer"]:
    if args.csv is not None:
        csvfile = open(args.csv, "w", newline="")
        csvwriter = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerow(["Name"] + TraceInfo.header())
    else:
        csvwriter = None

    return csvwriter


def open_trace(trace_path: Path) -> TextIO:
    if not trace_path.exists():
        print(f"Trace file does not exist: {trace_path}")
        exit(-1)

    if trace_path.suffix == ".gz":
        return gzip.open(trace_path, mode="rt")
    else:
        return open(trace_path, mode="r")


def print_to_csv(trace: Path, info: TraceInfo, csvwriter: Optional["_csv._writer"]) -> None:
    if csvwriter is not None:
        csvwriter.writerow([trace.name.split(".")[1]] + info.gather())


def print_to_term(info: TraceInfo) -> None:
    for (label, data) in zip(TraceInfo.header(), info.gather()):
        print(f"{label}: {data}")
    print()


#
# Program logic
#


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Produce a summary of Rust-related information from a Soong build trace")

    parser.add_argument(
        "traces", metavar="TRACE", type=ExtantPath, nargs="+", help="Soong trace file to process")
    parser.add_argument("--csv", default=None, help="Print results to CSV")

    return parser.parse_args()


def process_trace(trace_path: Path) -> TraceInfo:
    info = TraceInfo()
    with open_trace(trace_path) as fd:
        for item in json.load(fd):
            if "dur" in item:
                info.targets_total += 1
                info.duration_total_µs += item["dur"]

                rule_name = dig(item, "args", "tags", "rule_name")
                if rule_name:
                    if item["name"].startswith(TRACE_PATH_PREFIX):
                        if rule_name.startswith("cc"):
                            info.targets_cc += 1
                            info.duration_cc_µs += item["dur"]

                        elif rule_name.startswith("javac"):
                            info.targets_java += 1
                            info.duration_java_µs += item["dur"]

                        elif rule_name.startswith("rustc"):
                            info.targets_rust += 1
                            info.duration_rust_µs += item["dur"]

                        else:
                            info.targets_other += 1
                            info.duration_other_μs += item["dur"]

    return info


def main() -> None:
    args = parse_args()

    csvwriter = init_csv(args)

    for trace in args.traces:
        print(f"Processing {trace.name}\n")
        info = process_trace(trace)

        print_to_term(info)
        print_to_csv(trace, info, csvwriter)


if __name__ == "__main__":
    main()
