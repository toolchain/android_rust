#!/usr/bin/env python3
#
# Copyright (C) 2024 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Execute the build steps for a given branch and build target name"""

import argparse
import inspect
import os
import subprocess
import sys

import context

from android_rust.paths import BIN_PATH_PYTHON, WORKSPACE_PATH
from android_rust.target_definitions import TARGET_DEFS
from android_rust.utils import TERM_RED, ScriptException, dig, print_colored


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description=inspect.getdoc(sys.modules[__name__]))

    parser.add_argument("--dry-run", "-d", action="store_true", help="Don't run the command")

    parser.add_argument(
        "--list", "-l", action="store_true", help="List defined branches and their targets")

    parser.add_argument("branch", nargs="?", help="Branch to look for target in")
    parser.add_argument("target", nargs="?", help="Name of target to build")

    return parser.parse_args()


def main() -> None:
    args = parse_args()

    if args.list:
        print("")
        for branch_name in TARGET_DEFS:
            print(f"{branch_name}:")
            for target_name in TARGET_DEFS[branch_name]:
                print(f"\t{target_name}")
            print()

    else:
        if args.branch is None or args.target is None:
            raise ScriptException("Both the branch and target names must be specified")

        command_parts = dig(TARGET_DEFS, args.branch, args.target)
        if command_parts:
            command = [str(BIN_PATH_PYTHON)] + command_parts
            print(f"Command for {args.branch}/{args.target}:\n")
            print(" ".join(command))

            if args.dry_run:
                return
            elif subprocess.run(command, cwd=WORKSPACE_PATH).returncode != 0:
                raise ScriptException("Dispatched command failed during execution")
        else:
            raise ScriptException(
                f"No command found for branch {args.branch} and target {args.target}")


if __name__ == "__main__":
    try:
        main()
        sys.exit(0)

    except (ScriptException, argparse.ArgumentTypeError) as err:
        print_colored(str(err), TERM_RED)
        sys.exit(1)
