#!/usr/bin/env python3
#
# Copyright (C) 2022 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Execute the stages of the Rust Toolchain PGO pipeline locally"""

import argparse
from pathlib import Path
import shutil
import sys
from typing import TypedDict

import boltifyer
import build
import context
from test_compiler import ensure_bolt_profile_dir

from android_rust.paths import DIST_PATH_DEFAULT, OUT_PATH_PROFILES, PROFILE_SUBDIR_BOLT
from android_rust.toolchains import CLANG_TOOLCHAIN_HOST, export_profiles, merge_project_profiles
from android_rust.utils import (
    TEST_VERSION_NUMBER,
    ResolvedPath,
    ScriptException,
    prepare_prebuilts,
    run_build_target,
)


TARGETS = {
    "raven": "aosp_raven",
    "cuttlefish_x86": "aosp_cf_x86_64_phone",
    "cuttlefish_riscv": "aosp_cf_riscv64_phone"
}

COMMON_ARGS = [
    "--gc-sections",
    "--no-cargo-audit",
]


class BuildArgsDict(TypedDict):
    device_target: str
    device_target_variant: str
    prebuilt_version: str


#
# Program logic
#


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser("Build Rust prebuilts using the PGO pipeline")
    parser.add_argument(
        "--build-name",
        type=str,
        default="pgo-pipeline",
        help="Name used in the creation of various artifacts")
    parser.add_argument(
        "--dist",
        "-d",
        dest="dist_path",
        type=ResolvedPath,
        default=DIST_PATH_DEFAULT,
        help="Where to place distributable artifacts")

    parser.add_argument(
        "--lto",
        "-l",
        default="thin",
        choices=["none", "thin"],
        help="Type of LTO to perform. Valid LTO types: none, thin, full")
    parser.add_argument(
        "--llvm-linkage",
        default="shared",
        choices=["static", "shared"],
        help="Specify if LLVM should be built as a static or shared library")

    parser.add_argument(
        "--overwrite",
        "-o",
        action="store_true",
        help="Overwrite pre-existing PGO pipeline directories in DIST")
    parser.add_argument(
        "--start",
        "-s",
        type=float,
        choices=[1, 2, 2.5, 3, 4, 5, 5.5, 6],
        default=1,
        help="Which stage to start the pipeline from")
    parser.add_argument(
        "--end",
        "-e",
        type=float,
        choices=[1, 2, 2.5, 3, 4, 5, 5.5, 6],
        default=6,
        help="Which stage to end the pipeline at")

    args = parser.parse_args()
    COMMON_ARGS.append(f"--lto={args.lto}")
    COMMON_ARGS.append(f"--llvm-linkage={args.llvm_linkage}")

    return args


def main() -> None:
    args = parse_args()

    def setup_dist_instance(instance_path: str) -> Path:
        dist_path: Path = args.dist_path / f"rust-{args.build_name}" / instance_path
        if dist_path.exists():
            if args.overwrite:
                shutil.rmtree(dist_path)
            else:
                sys.exit(f"PGO Pipeline stage directory exists: {dist_path}")
        dist_path.mkdir(parents=True)

        return dist_path

    def test_dist_instance(instance_path: str) -> Path:
        dist_path: Path = args.dist_path / f"rust-{args.build_name}" / instance_path
        if dist_path.exists():
            return dist_path
        else:
            sys.exit(f"Required pipeline stage directory is missing: {dist_path}")

    #
    # Stage 1: Shared Linkage, Section GC, LTO, PGO Instrumented
    #

    if args.start <= 1 and 1 <= args.end:
        print("Executing Stage 1")
        dist_path_stage1 = setup_dist_instance("stage1")
        try:
            build.main(
                COMMON_ARGS + [
                    f"--dist={dist_path_stage1}",
                    f"--build-name={args.build_name}-stage1",
                    "--profile-generate",
                ])
        except:
            sys.exit("Failed to build Stage 1 compiler")

    elif args.end > 1:
        dist_path_stage1 = test_dist_instance("stage1")
        print("Re-using existing Stage 1")

    #
    # Stage 2: Gather profile data
    #

    dist_paths_stage2 = []
    if args.start <= 2 and 2 <= args.end:
        print("Executing Stage 2")
        prepare_prebuilts(dist_path_stage1 / f"rust-{args.build_name}-stage1.tar.xz", False)
        for short, long in TARGETS.items():
            dist_paths_stage2.append(setup_dist_instance(f"stage2/{short}"))
            build_args: BuildArgsDict = {
                "device_target": long,
                "device_target_variant": "userdebug",
                "prebuilt_version": TEST_VERSION_NUMBER,
            }
            run_build_target("clean", **build_args)
            try:
                run_build_target("droid", **build_args)
            except ScriptException as err:
                print(f"Failed to build Stage 2 target {short}")
                raise err
            export_profiles(CLANG_TOOLCHAIN_HOST, OUT_PATH_PROFILES, dist_paths_stage2[-1])
    elif args.end < 2:
        sys.exit()
    else:
        dist_paths_stage2 = [test_dist_instance(f"stage2/{short}") for short in TARGETS.keys()]
        print("Re-using existing Stage 2")

    #
    # Stage 2.5: Merge profiles 1
    #

    if args.start <= 2.5 and 2.5 <= args.end:
        print("Executing Stage 2.5")
        dist_path_merge1 = setup_dist_instance("stage2.5")
        merge_project_profiles(
            CLANG_TOOLCHAIN_HOST, [
                dist_path_stage1,
                *dist_paths_stage2,
            ], dist_path_merge1)

    elif args.end < 2.5:
        sys.exit()
    else:
        dist_path_merge1 = test_dist_instance("stage2.5")
        print("Re-using existing Stage 2.5")

    #
    # Stage 3: Shared Linkage, Section GC, LTO, PGO Use, with relocations
    #

    if args.start <= 3 and 3 <= args.end:
        print("Executing Stage 3")
        dist_path_stage3 = setup_dist_instance("stage3")
        try:
            build.main(
                COMMON_ARGS + [
                    f"--dist={dist_path_stage3}",
                    f"--build-name={args.build_name}-stage3",
                    f"--profile-use={dist_path_merge1}",
                    "--emit-relocs",
                ])
        except:
            sys.exit("Failed to build Stage 3 compiler")

    elif args.end < 3:
        sys.exit()
    else:
        dist_path_stage3 = test_dist_instance("stage3")
        print("Re-using existing Stage 3")

    #
    # Stage 4: Add BOLT instrumentation to the Stage 3 compiler
    #

    if args.start <= 4 and 4 <= args.end:
        print("Executing Stage 4")
        dist_path_stage4 = setup_dist_instance("stage4")
        try:
            boltifyer.boltify_toolchain(
                dist_path_stage3 / f"rust-{args.build_name}-stage3.tar.xz",
                dist_path_stage4,
                f"rust-{args.build_name}-stage4", # .tar.xz added by archive function
                strip_only=False,
                profile_generate=OUT_PATH_PROFILES)
        except Exception as err:
            print("Unable to BOLT instrument the Stage 3 compiler")
            raise err

    elif args.end < 4:
        sys.exit()
    else:
        dist_path_stage4 = test_dist_instance("stage4")
        print("Re-using existing Stage 4")

    #
    # Stage 5: Gather profile data from building target images
    #

    dist_paths_stage5 = []
    if args.start <= 5 and 5 <= args.end:
        print("Executing Stage 5")
        prepare_prebuilts(dist_path_stage4 / f"rust-{args.build_name}-stage4.tar.xz", False)
        for short, long in TARGETS.items():
            dist_paths_stage5.append(setup_dist_instance(f"stage5/{short}"))

            build_args: BuildArgsDict = {
                "device_target": long,
                "device_target_variant": "userdebug",
                "prebuilt_version": TEST_VERSION_NUMBER,
            }
            run_build_target("clean", **build_args)
            ensure_bolt_profile_dir(OUT_PATH_PROFILES / PROFILE_SUBDIR_BOLT)
            try:
                run_build_target("droid", **build_args)
            except Exception as err:
                print(f"Failed to build Stage 5 for target {short}")
                raise err

            export_profiles(
                CLANG_TOOLCHAIN_HOST,
                OUT_PATH_PROFILES / PROFILE_SUBDIR_BOLT,
                dist_paths_stage5[-1])

    elif args.end < 5:
        sys.exit()
    else:
        dist_paths_stage5 = [test_dist_instance(f"stage5/{short}") for short in TARGETS.keys()]
        print("Re-using existing Stage 5")

    #
    # Stage 5.5: Merge profiles 2
    #

    if args.start <= 5.5 and 5.5 <= args.end:
        print("Executing Stage 5.5")
        dist_path_merge2 = setup_dist_instance("stage5.5")
        merge_project_profiles(CLANG_TOOLCHAIN_HOST, dist_paths_stage5, dist_path_merge2)

    elif args.end < 5.5:
        sys.exit()
    else:
        dist_path_merge2 = test_dist_instance("stage5.5")
        print("Re-using existing Stage 5.5")

    #
    # Stage 6: Shared Linkage, Section GC, LTO, PGO Use, BOLT optimized
    #

    if args.start <= 6 and 6 <= args.end:
        print("Executing Stage 6")
        dist_path_stage6 = setup_dist_instance("stage6")
        boltifyer.boltify_toolchain(
            dist_path_stage3 / f"rust-{args.build_name}-stage3.tar.xz",
            dist_path_stage6,
            f"rust-{args.build_name}-stage6", # .tar.xz added by archive function
            strip_only=False,
            profile_use=dist_path_merge2)


if __name__ == "__main__":
    main()
    sys.exit(0)
