# Build Environment Setup

These instructions will allow you to initialize an Android Rust toolchain build
environment.  You only need to execute these instructions when you want to
check out a new copy of the project source repository.

## Download Source

The Rust toolchain has two branch manifests in AOSP, `rust-toolchain` and
`main-plus-rust`. We will be using the `main-plus-rust` branch in this
documentation for two reasons: 1) testing the prebuilts to ensure they can
build Android and 2) generating profiles for Profile Guided Optimization.

The following commands will create a repo directory (e.g.
`~/main-plus-rust`), initialize it with the main-plus-rust manifest,
synchronize the repository, and tell Git how to find the list of commit
revisions to skip when computing blame.

```shell
$ mkdir main-plus-rust
$ cd main-plus-rust
$ REPO_ALLOW_SHALLOW=0 repo init -u https://android.googlesource.com/platform/manifest -b main-plus-rust --use-superproject --partial-clone --partial-clone-exclude=platform/frameworks/base --clone-filter=blob:limit=10M
$ repo sync -j16
$ git config blame.ignoreRevsFile .git-blame-ignore-revs
```

## Configuring Tooling

To allow editors like VSCode and other tools to find the project's Python code
you can add the line below to the `toolchains/android_rust/.env` file:

```shell
PYTHONPATH=$PYTHONPATH:src/
```

The Android Rust toolchain uses the MyPy and YAPF tools to handle linting and
autoformatting respectively.  You can install these by running

`sudo apt install mypy yapf`

Finally, there are multiple ways to handle pre-commit hooks for the Android
Rust toolchain directory.  One option is to place the following script at
`toolchains/android_rust/.git/hooks/precommit`:

```shell
#!/usr/bin/sh

yapf -i tools/*.py src/android_rust/*.py src/binary_crates/*.py
mypy .
```
