# Android Rust Prebuilts

[go/android-rust-toolchain-team-prebuilts](http://go/android-rust-toolchain-team-prebuilts)

- [Android Platform](https://android.googlesource.com/platform/)
  - Current: v1.83.0
  - Source: Look for 'RustDefaultVersion' [code](https://source.corp.google.com/h/android/platform/build/soong/+/master:rust/config/global.go)
  - Contact: android-rust-toolchain

- [Android Emulator](https://developer.android.com/studio/run/emulator)
  - Current: v1.73.0 (pinned with https://android-review.git.corp.google.com/c/platform/manifest/+/2853565)
  - Source: Look for 'rust' in [code](https://cs.android.com/android/platform/superproject/+/emu-master-dev:external/qemu/android/build/toolchains.json)
  - Contact: jansene

- [Trusty](https://source.android.com/docs/security/features/trusty)
  - Current: v1.82.0
  - Source: Look for 'RUST_BINDIR' in [code](https://android.googlesource.com/trusty/vendor/google/aosp/+/master/scripts/envsetup.sh)
  - Contact: mikemcternan

- [Kernel]()
  - Current: v1.82.0
  - Source: Look at the version in [common/build.config.constants](https://cs.android.com/android/kernel/superproject/+/common-android-mainline:common/build.config.constants)
  - Contact: mmaurer
  - Once launched, will involve a version that moves at pace of upstream, may be done using its own branch to skip branch deletions.

- [Core]()
  - Current: 1.82.0
  - Source: Look at version under third_party [code](https://source.corp.google.com/piper///depot/google3/third_party/android/rust_toolchain/stable/METADATA;l=1?q=third_party%2Fandroid%2Frust_toolchain%2Fstable%2FMETA&sq=package:piper%20file:%2F%2Fdepot%2Fgoogle3%20-file:google3%2Fexperimental)
  - Contact: trybka

- [Generic Bootloader]()
  - Current: 1.81.0
  - Source: Look for 'GBL_RUST_VERSION' in [gbl_workspace_util.bzl](https://android.googlesource.com/platform/bootable/libbootloader/+/refs/heads/main/gbl/toolchain/gbl_workspace_util.bzl)
  - Contact: zyecheng
