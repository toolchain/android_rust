# Android Rust Toolchain

This repository contains the code and data necessary to build and test a Rust
toolchain for the Android Open Source Project.  Detailed information on several
topics can be found in our `documents/` directory:

* [Initializing the Rust Toolchain development environment](./documents/SETUP.md)
* [Common build and testing tasks](./documents/DEVELOPMENT.md)
* [Debugging build failures](./documents/DEBUGGING.md)
* [Release process steps](./documents/RELEASE.md)
* [Infrastructure build details](./documents/BUILDS.md)
* [Downstream clients](./documents/PREBUILTS.md)

## Supported Architectures and Targets

The Android Rust toolchain can generate code for the following architectures:

* `aarch64`
* `arm`
* `RISCV`
* `x86`
* `x86_64`

In addition, prebuilt standard libraries are available for the following target
triples:

* `aarch64-linux-android`
* `aarch64-unknown-none`
* `aarch64-unknown-uefi`
* `armv7-linux-androideabi`
* `armv7a-none-eabi`
* `i686-linux-android`
* `i686-unknown-linux-gnu`
* `i686-unknown-linux-musl`
* `i686-unknown-uefi`
* `riscv32i-unknown-none-elf`
* `riscv32imc-unknown-none-elf`
* `x86_64-linux-android`
* `x86_64-pc-windows-gnu`
* `x86_64-unknown-linux-gnu`
* `x86_64-unknown-linux-musl`
* `x86_64-unknown-uefi`

## Reproducing Android Toolchains

Starting with Rust 1.56.1 it is possible to reproduce Android's Rust toolchain
prebuilts. To do this you will start with the prebuilt's manifest, which is
located in the `prebuilts/rust/linux-x86/<version>` directory. Starting with
Rust 1.61.0 the manifest is also available in the
`toolchain/android_rust/artifacts/<version>` directory.

To begin reproducing a given build navigate to a new directory and run the
following command:

```shell
$ repo init -m /path/to/manifest.xml -b rust-toolchain && repo sync -c -j16
```

To reproduce toolchains before Rust version 1.61.0 run the following command:

```shell
$ ./toolchain/android_rust/tools/build.py --lto thin --llvm-linkage shared
```

To reproduce toolchains version 1.61.0 and above use the build commands listed
in the
`toolchain/android_rust/artifacts/<version>/rust_build_command.<target>.<build
id>.sh` files. Be sure to replace the paths to the PGO profiles from the saved
commands with paths to the profiles in the `artifacts` directory.

## Contact

This project is developed and maintained by the android-rust-toolchain@ team
and the best way to contact us is the team email alias.
